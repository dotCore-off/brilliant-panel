<?php
                                                                                                                                                                                                                                                                                                                        include("config.php");
/*
 *                  Bienvenue dans le fichier db.php
 * Ce fichier permet de configurer les informations de connexion à la BDD
 *        Référez vous à la documentation pour plus d'explications
 */

// Démarrage du système de session
session_start();

/*
 * Début de la configuration
 */

/*
 * Variable $BDD_HOST
 * @type = string
 * localhost / adresse IP
 */
$BDD_HOST = 'localhost';

/*
 * Variable $BDD_USERNAME
 * @type = string
 */
$BDD_USERNAME = 'root';

/*
 * Variable $BDD_PASS
 * @type = string
 */
$BDD_PASS = 'root';

/*
 * Variable $BDD_DB
 * @type = string
 */
$BDD_DB = 'test';

/*
 * Fin de la configuration
 */

// Connexion à la base de données
try {
    $bdd = new PDO('mysql:host='.$BDD_HOST.';dbname='.$BDD_DB.';charset=utf8', ''.$BDD_USERNAME.'', ''.$BDD_PASS.'');
}

// Récupération + Affichage des erreurs
catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}