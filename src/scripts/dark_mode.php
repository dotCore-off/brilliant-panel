<?php /** @noinspection ALL */

// On inclut le fichier de configuration
include_once('../configuration/db.php');

// On récupère la variable
$getMode = $_GET['mode'];
$previousUrl = $_GET['url'];

// Si session invalide > Login
if (!isset($_SESSION['username'])) {
    header('Location: ../login.php');
}

// On change la variable de session Theme + Redirections
elseif (isset($_SESSION['username']) and isset($_SESSION['theme']) and isset($_GET['mode'])) {
    if ($getMode == "dark") {
        $_SESSION['theme'] = true;
        header('Location: ..'.$previousUrl);
    }
    elseif ($getMode == "light") {
        $_SESSION['theme'] = false;
        header('Location: ..'.$previousUrl);
    }
}

else {
    header('Location ../blank.php');
}