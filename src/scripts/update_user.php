<?php /** @noinspection ALL */

// On inclut le fichier de configuration
include_once('../configuration/db.php');

// Si session invalide > Login
if (!isset($_SESSION['username'])) {
    header('Location: ../login.php');
}

// Vérification du niveau de privilège
if (!isset($_SESSION['perm']) or $_SESSION['perm'] < 2) {
    header('Location: ../users.php');
}

// On vérifie la présence d'entrées utilisateur et leur contenu
if (!empty($_POST['mailInput']) and !empty($_POST['nameInput']) and !empty($_POST['passInput']) and !empty($_POST['permInput']) and !empty($_POST['nameVerification'])) {
    // Mise en variables des entrées utilisateur
    $mail = strip_tags($_POST['mailInput']);
    $username = strip_tags($_POST['nameInput']);
    $password = strip_tags($_POST['passInput']);
    $level = strip_tags($_POST['permInput']);
    $nameCorrespondance = strip_tags($_POST['nameVerification']);

    // Vérification du mot de passe - Longueur
    if (mb_strlen($password) < 8) {
        header('Location: ../users.php');
        exit();
    }

    // Vérification du mot de passe - Sécurité
    if ((!preg_match('#[a-z]#', $password)) || (!preg_match('#[A-Z]#', $password)) || (!preg_match('#[0-9]#', $password)) || (preg_match('#\*#', $password))) {
        header('Location: ../users.php');
        exit();
    }

    // Vérification du mail - @
    if (!preg_match('#@#', $mail)) {
        header('Location: ../users.php');
        exit();
    }

    // Vérification du mail - .
    if (!preg_match('#\.#', $mail)) {
        header('Location: ../users.php');
        exit();
    }

    // Vérification du niveau de privilèges accordé
    if (($level == 0) or ($level > 3) or (!$level)) {
        // On le rétablis au niveau de base
        $level = 1;
    }

    // Dans le cas où un utilisateur essaye de donner des perms plus élevées que les siennes
    if ($level > $_SESSION['perm']) {
        // On rétrograde d'un niveau
        $level = $level - 1;
    }

    // Construction de notre requête
    // On utilise : pour préparer la requête, c'est une méthode alternative aux ?
    $query = "UPDATE acp_users SET username = :username, mail = :mail, password = :password, niveau = :perm WHERE username = :user";

    // Prépation de la requête
    $stmt = $bdd->prepare($query);

    // On remplace les placeholders ci-dessus (:username, etc..) par les variables récupérées
    $stmt->bindValue(':username', $username, PDO::PARAM_STR);
    $stmt->bindValue(':mail', $mail, PDO::PARAM_STR);
    $stmt->bindValue(':password', $password, PDO::PARAM_STR);
    $stmt->bindValue(':perm', $level, PDO::PARAM_INT);
    $stmt->bindValue(':user', $nameCorrespondance, PDO::PARAM_STR);

    // On exécute la requête
    $stmt->execute();

    // Redirection sur la page des utilisateurs
    header('Location: ../users.php');

    // Fermeture du script
    exit();
}

else {
    // Redirection sur la page des utilisateurs
    header('Location: ../users.php');
}