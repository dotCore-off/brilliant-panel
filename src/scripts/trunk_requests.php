<?php 

// Intégration de la configuration
include_once('../configuration/db.php');

// Si session invalide > Login
if (!isset($_SESSION['username'])) {
    header('Location: ../login.php');
}

// Si session valide > Connexion
elseif ((!isset($_GET['session'])) and (isset($_SESSION['username']))) {
    header('Location: trunk_requests.php?session='.$_SESSION['username'].'');
}

else { 
    // Requête pour vider la table
    $query = "TRUNCATE TABLE acp_requests";

    // On prépare puis on exécute
    $trunkQuery = $bdd->prepare($query);
    $trunkQuery->execute();

    // Redirection sur la page users
    header('Location: ../requests.php');
}