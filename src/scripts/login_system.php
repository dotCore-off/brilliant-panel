<?php /** @noinspection ALL */

// On inclut le fichier de configuration
include_once('../configuration/db.php');

// On vérifie la présence d'entrées utilisateur
if (!empty($_POST['userInput']) and !empty($_POST['passInput'])) {
  // Mise en variables des entrées utilisateur
  $name = strip_tags($_POST['userInput']);
  $password = strip_tags($_POST['passInput']);
  
  // Préparation de la requête + Exécution
  $select = $bdd->prepare("SELECT * FROM acp_users WHERE username = :username");
  $select->execute(array(
    ':username' => $name
  ));

  // Mise en variable des identifiants attendus
  $row = $select->fetch();
  $true_password = $row['password'];
  $true_name = $row['username'];
  
  // Si les entrées utilisateur = Identifiants attendus
  if ($name == $true_name && $password == $true_password) {
    // Stockage du nom d'utilisateur
    $_SESSION['username'] = $row['username'];
    $_SESSION['perm'] = $row['niveau'];
      $_SESSION['theme'] = false;
    
    // Vérification mineure
    if (isset($_SESSION['username'])) {
      // Redirection sur le panel
      header('Location: ../redirect.php?session='.$_SESSION['username'].'');
    } else {
      // Redirection sur le login
      header('Location: ../login.php');
    }
 
  } else {
    // Redirection sur le login - TODO : Fix la redondance
    header('Location: ../login.php');
  }
 
 
} else {
  // Redirection sur le login
  header('Location: ../login.php');
}