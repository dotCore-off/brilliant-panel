<?php /** @noinspection ALL */

// On inclut le fichier de configuration
include_once('../configuration/db.php');

// Si session invalide > Login
if (!isset($_SESSION['username'])) {
    header('Location: ../login.php');
}

// Si session valide > Connexion
elseif ((!isset($_GET['session'])) and (isset($_SESSION['username']))) {
    header('Location: add_contact.php?session='.$_SESSION['username'].'');
}

// On vérifie la présence d'entrées utilisateur et leur contenu
if (!empty($_POST['addContactName']) and !empty($_POST['addContactUser']) and !empty($_POST['addContactNumint']) and !empty($_POST['addContactNumext'])) {
    // Mise en variables des entrées utilisateur
    $nom = strip_tags($_POST['addContactName']);
    $prenom = strip_tags($_POST['addContactUser']);
    $numint = strip_tags($_POST['addContactNumint']);
    $numext = strip_tags($_POST['addContactNumext']);
    $note = strip_tags($_POST['addContactNote']);

    // On remplace $note par NULL
    if (empty($note)) {
        $note = NULL;
    }

    // Construction de notre requête
    // Les ? représentent les places où seront assignées les variables lors de l'exécution
    $query = "INSERT INTO acp_contacts (nom, prenom, numero_int, numero_ext, note) VALUES(?, ?, ?, ?, ?)";

    // Prépation de la requête
    $stmt = $bdd->prepare($query);

    // On exécute la requête avec les variables qui remplacent les ?
    $stmt->execute([$nom, $prenom, $numint, $numext, $note]);

    // Redirection sur la page des utilisateurs
    header('Location: ../contacts.php');

    // Fermeture du script
    exit();
}

else {
    // Redirection sur la page des utilisateurs
    header('Location: ../contacts.php');
}