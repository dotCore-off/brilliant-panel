<?php /** @noinspection SqlNoDataSourceInspection */
// Intégration de la config
    include_once('../configuration/db.php');

    // Vérification de la session + URL
    if (!isset($_SESSION['username'])) {
        header('Location: ../login.php');
    }

    // Implémentation de la vérification de droits d'accès
    elseif (isset($_SESSION['username']) and $_SESSION['perm'] < 2) {
        header('Location: ../bdd.php?session='.$_SESSION['username'].'');
    }

    elseif (isset($_SESSION['username']) and $_SESSION['perm'] == 3) {
        // On vérifie la présence des entrées utilisateur et leur contenu
        if (!empty($_GET['name']) and !empty($_GET['mail']) and !empty($_GET['pass']) and !empty($_GET['priv'])) {
            // On stock les variables contenues dans le lien | On ajoute une petite suite à .bis au cas où
            $dupName = strip_tags($_GET['name']);
            $dupName .= '.bis';
            $dupMail = strip_tags($_GET['mail']);
            $dupPass = strip_tags($_GET['pass']);
            $dupPriv = strip_tags($_GET['priv']);

            // Vérification du niveau
            if (!isset($dupPriv)) {
                $dupPriv = 1;
            }

            // Vérification du nom
            if (!isset($dupName)) {
                $dupName = "Default.bis";
            }

            // Vérification du mail
            if (!isset($dupMail)) {
                $dupMail = "mail@default.bis";
            }

            // Vérification du MDP
            if (!isset($dupPass)) {
                $dupPass = "Root12345";
            }

            // Construction de notre requête
            // Les ? représentent les places où seront assignées les variables lors de execution
            $query = "INSERT INTO acp_users (username, mail, password, niveau) VALUES(?, ?, ?, ?)";

            // Préparation de la requête
            $stmt = $bdd->prepare($query);

            // On exécute la requête avec les variables qui remplacent les ?
            $stmt->execute([$dupName, $dupMail, $dupPass, $dupPriv]);

            // Redirection sur la page des utilisateurs
            header('Location: ../users.php?session='.$_SESSION['username'].'');

            // Fermeture du script
            exit();
        }

        // Si une des entrées est manquante 
        else {
            // On redirige
            header('Location: ../users.php?session='.$_SESSION['username'].'');
        }
    }