<?php

// On inclut le fichier de configuration
include_once('../configuration/db.php');

// Vérification de l'intégrité de l'entrée utilisateur
if (!empty($_POST['motdContentArea']) and isset($_SESSION['username']) and $_SESSION['perm'] == 3) {
    // Mise en variable
    $newMotd = $_POST['motdContentArea'];

    // Requête d'insertion du nouvel MOTD
    $query = "UPDATE acp_settings SET motd_content = :newmotd";

    // Préparation + Bind + Exécution de la requête
    $stmt = $bdd->prepare($query);
    $stmt->bindValue(':newmotd', $newMotd);
    $stmt->execute();

    // Redirection sur la page des paramètres
    header('Location: ../settings.php?session='.$_SESSION['username']);
}

else {
    header('Location: ../login.php');
}