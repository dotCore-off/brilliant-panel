<?php /** @noinspection ALL */

// On inclut le fichier de configuration
include_once('../configuration/db.php');

// Vérification de tous les champs
if (!empty($_POST['hostBdd']) and !empty($_POST['userBdd']) and !empty($_POST['passBdd']) and !empty($_POST['dbBdd'])) {
    // Nouvelles valeurs -> Entrées utilisateur
    $newHost = strip_tags($_POST['hostBdd']);
    $newUser = strip_tags($_POST['userBdd']);
    $newPass = strip_tags($_POST['passBdd']);
    $newDb = strip_tags($_POST['dbBdd']);

    // Anciennes valeurs -> Config.php
    $oldHost = $BDD_HOST;
    $oldUser = $BDD_USERNAME;
    $oldPass = $BDD_PASS;
    $oldDb = $BDD_DB;

    // Vérification du mot de passe - Longueur
    if (mb_strlen($newPass) < 8) {
        header('Location: ../bdd.php');
        exit();
    }

    // Vérification du mot de passe - Sécurité
    if ((!preg_match('#[a-z]#', $newPass)) || (!preg_match('#[A-Z]#', $newPass)) || (!preg_match('#[0-9]#', $newPass)) || (preg_match('#\*#', $newPass))) {
        header('Location: ../bdd.php'); 
        exit();
    }

    // Vérification de l'hôte indiqué
    if (!preg_match('#[\.]|[0-9]', $newHost) && (!preg_match('#localhost#', $newHost))) {
        header('Location: ../index.php');
        exit();
    }

    // Tentative de connexion à la nouvelle base de données
    try {
        $newBdd = new PDO('mysql:host='.$newHost.';dbname='.$newDb.';charset=utf8', ''.$newUser.'', ''.$newPass.'');
    }

    // En cas d'erreur, on arrête
    catch (Exception $e) {
        header('Location: ../users.php');
        exit();
    }

    // Remplacement des informations de connexion - Host
    if ($newHost != $oldHost) {
        // Ouverture du fichier
        $lines = file('../dev/config.php');
        $pattern = ''.$oldHost.'';

        // Boucle de remplacement
        foreach ($lines as $line) {
            if ($line == $pattern) {
                $replaceVar = str_replace($pattern, $newHost, $line);
                header('Location: index.php');
            }
        }
    }
}

// Redirection si champs mal remplis
else {
    header('Location: ../bdd.php');
}