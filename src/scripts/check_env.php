<!-- Script - Vérification de l'environnement -->
<script>
    // Fonction principale
    function checkEnv() {
        // Si le site est lancé en local
        if(window.location.protocol === 'file:') {
            // Notification via TataJS
            tata.error('AdminCP - Notification', "Attention : il semblerait que le site soit exécuté en local, certaines fonctionnalités peuvent ne pas être disponible !", {
                position: 'br',
                duration: 7000,
                closeBtn: true,
                progress: true,
                animation: 'fade',
                onClick: null,
                onClose: null
            })
            return 0; 
        }
        // Si le site est lancé depuis un serveur localhost
        if(!window.location.host.replace(/localhost|127\.0\.0\.1/i, '')) { 
            // Notification via TataJS
            tata.info('AdminCP - Notification', "Attention : certaines fonctionnalités du site peuvent être limitées car vous l'exécutez en local !", {
                position: 'br',
                duration: 7000,
                closeBtn: true,
                progress: true,
                animation: 'fade',
                onClick: null,
                onClose: null
            })
            return 2; 
        }
        // Si le site est lancé depuis un serveur distant + fichier distant
        // Notification via TataJS
        tata.info('Bienvenue sur AdminCP', "Nous vous souhaitons une agréable navigation !", {
            position: 'br',
            duration: 7000,
            closeBtn: true,
            progress: true,
            animation: 'fade',
            onClick: null,
            onClose: null
        })
        return 1;
    }

    // Fonction principale
    function setTheme() {
        // Si le site est lancé en local
        <?php
        $getTheme = $_SESSION['theme'];

        // Si le thème est sombre
        if ($getTheme == true) {
        ?>

        // On récupère des éléments dits "individuels" via leurs IDs
        let footer = document.getElementById("footerBp");
        let topbar = document.getElementById("topbarBp");
        let bg = document.getElementById("content-wrapper");
        let supportBtn = document.getElementById("discBtn");
        let userDD = document.getElementById("userExpandDropdown");

        // On récupère tous les éléments qui possèdent les classes inscrites ci-dessous
        // Cela retourne une HTMLCollection()
        let cards = document.getElementsByClassName("card");
        let headers = document.getElementsByClassName("card-header");
        let texts = document.getElementsByClassName("text-gray-800");


        // On loupe à travers les cartes
        for (let i = 0; i < cards.length; i++) {
            cards[i].classList.toggle("dark-mode-cards");
            cards[i].classList.replace("border-bottom-dark", "border-bottom-primary");
            cards[i].classList.replace("border-top-dark", "border-top-primary");
            cards[i].classList.replace("border-left-dark", "border-left-primary");
            cards[i].classList.replace("border-right-dark", "border-right-primary");
            cards[i].classList.remove("border");
        }

        // On loop à travers les divers textes
        for (let i = 0; i < texts.length; i++) {
            texts[i].classList.remove("text-gray-800");
        }

        // On loop à travers les headers de carte
        for (let i = 0; i < headers.length; i++) {
            headers[i].classList.toggle("dark-mode-cards");
        }

        // Quelques modifications "individuelles"
        footer.classList.toggle("dark-mode-footer");
        topbar.classList.toggle("dark-mode-topbar");
        userDD.classList.toggle("dark-mode-topbar");
        bg.classList.toggle("dark-mode-bg");
        supportBtn.classList.replace("btn-outline-dark", "btn-outline-info");

        <?php
        }
        // Si le thème est clair
        elseif ($getTheme == false) {?>
            // On ne fait rien
            // Le thème par défaut est clair
        <?php } ?>
    }

    // Récupération de la page actuelle
    let currentLocation = document.location.href;

    // Si le site est chargé
    if (document.readyState === 'complete') {
        // On exécute checkEnv() au bout de deux secondes
        setTimeout(setTheme, 10);
        if (currentLocation.includes("login.php")) {
            setTimeout(checkEnv, 2000);
        }
    // Sinon
    } else {
        // On ajoute un listener
        document.addEventListener('DOMContentLoaded', function() {
            // On exécute checkEnv() au bout de deux secondes
            setTimeout(setTheme, 10);
            if (currentLocation.includes("login.php")) {
                setTimeout(checkEnv, 2000);
            }
        });
    }
</script>