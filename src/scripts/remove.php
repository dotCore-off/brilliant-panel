<?php /** @noinspection PhpUndefinedVariableInspection */
// Intégration de la config
    include_once('../configuration/db.php');

    // Requête pour dénombrer les utilisateurs
    $countQuery = "SELECT COUNT(username) FROM acp_users";

    // Préparation + Exécution
    $getUsers = $bdd->prepare($countQuery);
    $getUsers->execute();

    // Récupération du résultat + Conversion pour l'affichage
    $results = $getUsers->fetch(PDO::FETCH_NUM);
    $counts = $results[0];

    // Vérification de la session + URL
    if (!isset($_SESSION['username'])) {
        header('Location: ../login.php');
    }

    // Implémentation de la vérification de droits d'accès
    elseif (isset($_SESSION['username']) and $_SESSION['perm'] < 3) {
        header('Location: ../users.php?session='.$_SESSION['username'].'');
    }

    // Si session valide + permissions suffisantes + suffisamment d'utilisateur
    elseif (isset($_SESSION['username']) and $_SESSION['perm'] == 3 and $counts > 1) {
        $concernedUser = strip_tags($_GET['name']);

        // Préparation de la requête + Exécution
        $delete = "DELETE FROM acp_users WHERE username = '".$concernedUser."'";

        // Préparation de la requête
        $stmt = $bdd->prepare($delete);

        // On exécute la requête avec les variables qui remplacent les ?
        $stmt->execute();
    }

    // On déconnecte si l'utilisateur actif vient d'être supprimer
    if (isset($_SESSION['username']) and $_SESSION['username'] == $concernedUser) {
        header('Location: disconnect.php');
        exit(0);
    }

    // Sinon
    else {
        header('Location: ../users.php?session='.$_SESSION['username'].'');
    }