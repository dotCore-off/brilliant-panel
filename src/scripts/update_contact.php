<?php /** @noinspection ALL */

// On inclut le fichier de configuration
include_once('../configuration/db.php');

// Si session invalide > Login
if (!isset($_SESSION['username'])) {
    header('Location: ../login.php');
}

// Vérification du niveau de privilège
if (!isset($_SESSION['perm'])) {
    header('Location: ../contacts.php');
}

// On vérifie la présence d'entrées utilisateur et leur contenu
if (!empty($_POST['nomInput']) and !empty($_POST['prenomInput']) and (!empty($_POST['numIntInput']) or !empty($_POST['numExtInput']))) {
    // Mise en variables des entrées utilisateur
    $nom = strip_tags($_POST['nomInput']);
    $prenom = strip_tags($_POST['prenomInput']);
    $originalUser = strip_tags($_POST['nameVerification']);

    if (!empty($_POST['numIntInput'])) {
        $numInt = strip_tags($_POST['numIntInput']);
    }

    if (!empty($_POST['numExtInput'])) {
        $numExt = strip_tags($_POST['numExtInput']);
    }

    if (!empty($_POST['noteInput'])) {
        $note = strip_tags($_POST['noteInput']);
    }

    // Construction de notre requête
    // On utilise : pour préparer la requête, c'est une méthode alternative aux ?
    $query = "UPDATE acp_contacts SET nom = :nom, prenom = :prenom, numero_int = :numInt, numero_ext = :numExt, note = :note WHERE nom = :user";

    // Prépation de la requête
    $stmt = $bdd->prepare($query);

    // On remplace les placeholders ci-dessus (:username, etc..) par les variables récupérées
    $stmt->bindValue(':nom', $nom, PDO::PARAM_STR);
    $stmt->bindValue(':prenom', $prenom, PDO::PARAM_STR);
    $stmt->bindValue(':numInt', $numInt, PDO::PARAM_STR);
    $stmt->bindValue(':numExt', $numExt, PDO::PARAM_STR);
    $stmt->bindValue(':note', $note, PDO::PARAM_STR);
    $stmt->bindValue(':user', $originalUser, PDO::PARAM_STR);

    // On exécute la requête
    $stmt->execute();

    // Redirection sur la page des utilisateurs
    header('Location: ../contacts.php');

    // Fermeture du script
    exit();
}

else {
    // Redirection sur la page des utilisateurs
    header('Location: ../contacts.php');
}