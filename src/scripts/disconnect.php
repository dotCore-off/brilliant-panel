<?php

// Ouverture du système de sessions
session_start();

// Suppression de la session active
session_unset();

// Destruction du système de session
session_destroy();

// Redirection sur la page de login
header('Location: ../login.php');