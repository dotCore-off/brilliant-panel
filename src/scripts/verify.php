<!-- Scripts - Contrôle de formulaire -->
<script>
    // Fonction qui permet de vérifier le contenu d'un champ
    function refreshContent() {
        // On récupère l'URL courante
        let currentUrl = document.location.href;

        // Si l'on se trouve sur la page settings.php
        if (currentUrl.includes("settings.php")) {
            // Récupération du bouton + de l'input
            let motdContent = document.getElementById("motdArea").value.length;
            const motdBtn = document.getElementById("motdSubmitBtn");

            // On active le bouton si la longueur minimale est respectée
            motdBtn.disabled = motdContent <= 20;
        }

        // Si l'on se trouve sur la page bdd.php
        if (currentUrl.includes("bdd.php")) {
            // Récupération des entrées + des submits
            let hostContent = document.getElementById("hostInput").value.length;
            let userContent = document.getElementById("userInput").value.length;
            let passContent = document.getElementById("passInput").value.length;
            let tableContent = document.getElementById("tableInput").value.length;
            const bddBtn = document.getElementById("bddSubmitBtn");

            // On active le bouton si chaque champ possède au minimum 1 caractère
            bddBtn.disabled = !(hostContent > 0 && userContent > 0 && passContent > 0 && tableContent > 0);
        }

        // Si l'on se trouve sur la page bdd.php
        if (currentUrl.includes("edit.php")) {
            // Récupération des entrées + des submits
            let mailContent = document.getElementById("formInputMail").value.length;
            let nameContent = document.getElementById("formInputName").value.length;
            let passContent = document.getElementById("formInputPass").value.length;
            let permContent = document.getElementById("formInputPerm").value.length;
            const updBtn = document.getElementById("updateSubmitBtn");

            // On active le bouton si chaque champ possède au minimum 1 caractère
            updBtn.disabled = !(mailContent > 0 && nameContent > 0 && passContent > 0 && permContent > 0);
        }

        // Si l'on se trouve sur la page login.php
        if (currentUrl.includes("login.php")) {
            // Récupération des entrées
            let userContent = document.getElementById("inputLoginUsername").value.length;
            let passContent = document.getElementById("inputLoginPassword").value.length;

            // Pour celle-ci, on ne récupère que la valeur puisque l'on doit faire une comparaison pour connaître son contenu
            let resetContent = document.getElementById("userConcerned").value;

            // Récupération des boutons de soumission
            const loginBtn = document.getElementById("loginSubmitBtn");
            const resetBtn = document.getElementById("sendResetBtn");

            /* On active les bouton si les conditions suivantes sont respectées :
                - Bouton de login :
                    > minimum 1 caractère pour le nom d'utilisateur
                    > minimum 8 caractères pour le mot de passe (on autorise 8 caractères minimum pour la création d'un utilisateur)
                - Bouton de demande de réinitialisation de mot de passe :
                    > minmum 1 caractère + minimum 1 @ et 1 . (vu que c'est une adresse mail)
            */
            loginBtn.disabled = !(userContent > 0 && passContent > 7);
            resetBtn.disabled = !(resetContent.length > 0 && resetContent.includes("@") && resetContent.includes("."));
        }

        // Si l'on se trouve sur la page users.php
        if (currentUrl.includes("users.php")) {
            // Formulaire d'ajout d'un utilisateur
            // Récupération des entrées
            let newMailContent = document.getElementById("newUserMail").value;
            let newUserContent = document.getElementById("newUserName").value.length;
            let newPassContent = document.getElementById("newUserPass").value.length;
            let newPermContent = document.getElementById("newUserPerm").value;

            // Récupération des boutons de soumission
            const addUserBtn = document.getElementById("confirmUserBtn");

            /* On active les bouton si les conditions suivantes sont respectées :
                - Bouton d'ajout d'un utilisateur :
                    > minimum 1 caractère pour le nom d'utilisateur
                    > minimum 8 caractères pour le mot de passe (on autorise 8 caractères minimum pour la création d'un utilisateur)
                - Bouton de demande de réinitialisation de mot de passe :
                    > minmum 1 caractère + minimum 1 @ et 1 . (vu que c'est une adresse mail)
            */
            addUserBtn.disabled = !(newUserContent > 0 && newPassContent > 0 && newPermContent.length > 0 && newPermContent.length < 2);
            addUserBtn.disabled = !(newMailContent.length > 0 && newMailContent.includes("@") && newMailContent.includes("."));
            addUserBtn.disabled = !(newPermContent.includes("1") || newPermContent.includes("2") || newPermContent.includes("3"));
        }
    }

    // Si le site est chargé
    if (document.readyState === 'complete') {
        // On exécute la fonction au bout de deux secondes
        setTimeout(refreshContent, 500)
        // Sinon
    } else {
        // On ajoute un listener
        document.addEventListener('DOMContentLoaded', function() {
            // On exécute la fonction au bout de deux secondes
            setTimeout(refreshContent, 500)
        });
    }
</script>