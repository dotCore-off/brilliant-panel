<!-- Scripts - Login -->
<script>
    // Notification d'erreur
    function cancelNotif() {
        // Notification via TataJS
        tata.error('AdminCP - Notification', 'Vous avez annulé votre demande.', {
            position: 'br',
            duration: 5000,
            closeBtn: false,
            progress: true,
            animation: 'slide',
            onClick: null,
            onClose: null
        })
    }

    // Notification d'envoi
    function envoiNotif() {
        // Notification via TataJS
        tata.success('AdminCP - Notification', 'Votre demande a correctement été transmise à notre équipe.', {
            position: 'br',
            duration: 7000,
            closeBtn: false,
            progress: true,
            animation: 'slide',
            onClick: null,
            onClose: null
        })
    }

    // Notification d'opérateur
    function opeNotif() {
        // Notification via TataJS
        tata.info('AyayaCP - Notification', "Cette fonctionnalité n'existe pas encore, réessayez ultérieurement.", {
            position: 'tr',
            duration: 5000,
            closeBtn: false,
            progress: true,
            animation: 'slide',
            onClick: null,
            onClose: null
        })
    }

    // Fonction d'affichage du succès des vérifs d'info
    function successNotif() {
        // Notification via TataJS
        tata.success('AyayaCP - Connexion', "Connexion validée, redirection...", {
            position: 'tr',
            duration: 1000,
            closeBtn: false,
            progress: false,
            animation: 'slide',
            onClick: null,
            onClose: null
        })
            
        // Lancement de la redirection
        setTimeout(redirectPanel, 1000);
    }

    // Fonction de redirection
    function redirectPanel() {
        // Redirection sur la page du panel
        document.location.href="index.php" 
    }

    // Fonction d'affichage du succès des vérifs d'info
    function ajoutNotif() {
        // Notification via TataJS
        tata.success('AyayaCP - Utilisateurs', "Un nouvel utilisateur a été ajouté !", {
            position: 'br',
            duration: 7000,
            closeBtn: false,
            progress: true,
            animation: 'slide',
            onClick: null,
            onClose: null
        })
    }
        
    // Fonction d'attente
    function delayLogin() {
        // Notification via TataJS
        tata.info('AyayaCP - Connexion', "Vérification des informations...", {
            position: 'br',
            duration: 2000,
            closeBtn: false,
            progress: false,
            animation: 'slide',
            onClick: null,
            onClose: null
        })

        // Notification de succès
        setTimeout(successNotif, 2000);
    }
</script>