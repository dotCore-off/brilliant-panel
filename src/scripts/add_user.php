<?php /** @noinspection ALL */

// On inclut le fichier de configuration
include_once('../configuration/db.php');

// Si session invalide > Login
if (!isset($_SESSION['username'])) {
    header('Location: ../login.php');
}

// Si session valide > Connexion
elseif ((!isset($_GET['session'])) and (isset($_SESSION['username']))) {
    header('Location: add_user.php?session='.$_SESSION['username'].'');
}

// Vérification du niveau de privilège
if (!isset($_SESSION['perm']) or $_SESSION['perm'] < 2) {
    header('Location: ../users.php');
}

// On vérifie la présence d'entrées utilisateur et leur contenu
if (!empty($_POST['addMail']) and !empty($_POST['addUser']) and !empty($_POST['addPass']) and !empty($_POST['addLevel'])) {
    // Mise en variables des entrées utilisateur
    $mail = strip_tags($_POST['addMail']);
    $username = strip_tags($_POST['addUser']);
    $password = strip_tags($_POST['addPass']);
    $level = strip_tags($_POST['addLevel']);

    // Vérification du mot de passe - Longueur
    if (mb_strlen($password) < 8) {
        header('Location: ../users.php');
        exit();
    }

    // Vérification du mot de passe - Sécurité
    if ((!preg_match('#[a-z]#', $password)) || (!preg_match('#[A-Z]#', $password)) || (!preg_match('#[0-9]#', $password)) || (preg_match('#\*#', $password))) {
        header('Location: ../users.php'); 
        exit();
    }

    // Vérification du mail - @
    if (!preg_match('#@#', $mail)) {
        header('Location: ../users.php');
        exit();
    }

    // Vérification du mail - .
    if (!preg_match('#\.#', $mail)) {
        header('Location: ../users.php');
        exit();
    }

    // Vérification du niveau de privilèges accordé
    if (($level == 0) or ($level > 3) or (!$level)) {
        // On le rétablis au niveau de base
        $level = 1;
    }

    // Construction de notre requête
    // Les ? représentent les places où seront assignées les variables lors de l'exécution
    $query = "INSERT INTO acp_users (username, mail, password, niveau) VALUES(?, ?, ?, ?)";

    // Prépation de la requête
    $stmt = $bdd->prepare($query);

    // On exécute la requête avec les variables qui remplacent les ?
    $stmt->execute([$username, $mail, $password, $level]);

    // Redirection sur la page des utilisateurs
    header('Location: ../users.php');

    // Fermeture du script
    exit();
}

else {
    // Redirection sur la page des utilisateurs
    header('Location: ../users.php');
}