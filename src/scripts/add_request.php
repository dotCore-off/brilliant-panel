<?php 

// On inclut le fichier de configuration
include_once('../configuration/db.php');

// Vérification de l'intégrité de l'entrée utilisateur
if (!empty($_POST['userConcerned'])) {
    // Mise en variables
    $userRequest = strip_tags($_POST['userConcerned']);
    $currentDate = date('Y/m/d');
    $setState = 0; 

    // Requête de création de demande
    $query = "INSERT INTO acp_requests (email, date, traitee) VALUES(?, ?, ?)";

    // Préparation + Exécution de la requête
    $stmt = $bdd->prepare($query);
    $stmt->execute([$userRequest, $currentDate, $setState]);
}

// Redirection sur la page de connexion
header('Location: ../login.php');