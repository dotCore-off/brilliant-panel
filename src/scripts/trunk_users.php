<?php 

// Intégration de la configuration
include_once('../configuration/db.php');

// Si session invalide > Login
if (!isset($_SESSION['username'])) {
    header('Location: ../login.php');
}

// Si session valide > Connexion
elseif ((!isset($_GET['session'])) and (isset($_SESSION['username']))) {
    header('Location: trunk_users.php?session='.$_SESSION['username'].'');
}

else { 
    // On vérifie la présence d'entrées utilisateur et leur contenu
    if (!empty($_POST['addMail2']) and !empty($_POST['addUser2']) and !empty($_POST['addPass2']) and !empty($_POST['addLevel2'])) {
        // Mise en variables des entrées utilisateur
        $mail = strip_tags($_POST['addMail2']);
        $username = strip_tags($_POST['addUser2']);
        $password = strip_tags($_POST['addPass2']);
        $level = strip_tags($_POST['addLevel2']);

        // Vérification du mot de passe - Longueur
        if (mb_strlen($password) < 8) {
            header('Location: ../users.php');
            exit();
        }

        // Vérification du mot de passe - Sécurité
        if ((!preg_match('#[a-z]#', $password)) || (!preg_match('#[A-Z]#', $password)) || (!preg_match('#[0-9]#', $password)) || (preg_match('#\*#', $password))) {
            header('Location: ../users.php'); 
            exit();
        }

        // Vérification du mail - @
        if (!preg_match('#@#', $mail)) {
            header('Location: ../users.php');
            exit();
        }

        // Vérification du mail - . (test)
        if (!preg_match('#\.#', $mail)) {
            header('Location: ../users.php');
            exit();
        }

        // Vérification du niveau de privilèges accordé
        if (($level == 0) or ($level > 3) or (!$level)) {
            // On le rétablis au niveau de base
            $level = 1;
        }

        // Requête pour vider la table
        $query = "TRUNCATE TABLE acp_users";

        // On prépare puis on exécute
        $trunkQuery = $bdd->prepare($query);
        $trunkQuery->execute();

        // Construction de notre requête pour l'ajout d'utilisateur
        // Les ? représentes les places où seront assignées les variables lors de l'exécution
        $userQuery = "INSERT INTO acp_users (username, mail, password, niveau) VALUES(?, ?, ?, ?)";

        // Prépation de la requête
        $stmt = $bdd->prepare($userQuery);

        // On exécute la requête avec les variables qui remplacent les ?
        $stmt->execute([$username, $mail, $password, $level]);

        // Destruction de la session en cours
        session_unset();
        session_destroy();

        // Redirection sur la page users
        header('Location: ../login.php');
    }

    else {
        header('Location: ../users.php');
    }
}