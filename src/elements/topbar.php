<!-- Barre horizontale au top -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow" id="topbarBp">

<!-- Activation / Désactivation de la navbar (Pour mobile) -->
<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3" aria-label="Etendre la barre de navigation">
    <i class="fa fa-bars"></i>
</button>

<!-- Bouton pour la documentation (PC) - TODO : Documentation -->
<a href="#" class="btn btn-outline-primary shadow btnDoc">
    <i class="fa fa-book"></i>
    <span class="txtBut">Documentation</span>
</a>

<!-- Diviseur -->
<div class="topbar-divider d-none d-sm-block"></div>

<!-- Bouton pour le Support (PC) -->
<a href="https://discord.gg/" class="btn btn-outline-dark shadow btnDoc" id="discBtn">
    <i class="fab fa-discord"></i>
    <span class="txtBut">Support</span>
</a>

<!-- Topbar -->
<ul class="navbar-nav ml-auto">

    <!-- Barre de recherche (mobile) -->
    <li class="nav-item dropdown no-arrow d-sm-none">
        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
        </a>
        <!-- Messages -->
        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
            aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
                <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small"
                        placeholder="Rechercher..." aria-label="Rechercher"
                        aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </li>

    <!-- Diviseur -->
    <div class="topbar-divider d-none d-sm-block"></div>

    <?php 
        // Vérification de la variable + remplacement
        if (!isset($_GET['session'])) {
            $displayname = "Administrateur";
        }
        // Affichage normal si tout est bon
        else {
            $displayname = $_GET['session'];
        }

        // Vérification de la variable + remplacement
        if (!isset($_SESSION['theme'])) {
            $_SESSION['theme'] = false;
        }

        // Vérification de la variable + Remplacement
        if (!isset($_SESSION['avatar'])) {
            $_SESSION['avatar'] = "assets/img/avatar_user.png";
        }


        // On ajoute la position de la ressource actuelle à l'URL
        $currentUrl = $_SERVER['REQUEST_URI'];
    ?>


    <!-- Onglet - Utilisateur -->
    <li class="nav-item dropdown no-arrow userSelectMenu">
        <!-- Création d'un menu déroulant -->
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <!-- Nom d'utilisateur -->
            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $displayname ?></span>
            <!-- Avatar -->
            <img class="img-profile rounded-circle" src="<?php echo $_SESSION['avatar']?>" alt="Avatar de l'utilisateur">
        </a>
        <!-- Options disponibles - Menu déroulant -->
        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown" id="userExpandDropdown">
            <?php
                if ($_SESSION['perm'] == 3) {
            ?>
            <!-- Nouveau choix -->
            <a class="dropdown-item usr-dropdown" href="#">
                <i class="fas fa-list fa-sm fa-fw mr-2"></i>
                Logs
            </a>
            <?php
                }
            ?>

            <?php 
                // Mode clair
                if ($_SESSION['theme'] == false) {
            ?>
                <!-- Nouveau choix -->
                <a class="dropdown-item usr-dropdown" href="../scripts/dark_mode.php?session=<?php echo $_SESSION['username']?>&mode=dark&url=<?php echo $currentUrl?>">
                    <i class="fas fa-moon fa-sm fa-fw mr-2"></i>
                    Mode sombre
                </a>
            <?php 
                }
            ?> 
            
            <?php 
                // Mode sombre
                if ($_SESSION['theme'] == true) {
            ?>
                <!-- Nouveau choix -->
                <a class="dropdown-item usr-dropdown" href="../scripts/dark_mode.php?session=<?php echo $_SESSION['username']?>&mode=light&url=<?php echo $currentUrl?>">
                    <i class="fas fa-sun fa-sm fa-fw mr-2"></i>
                    Mode clair
                </a>
            <?php 
                }
            ?>

            <?php
                if ($_SESSION['perm'] == 3) {
            ?>
            <!-- Nouveau choix -->
            <a class="dropdown-item usr-dropdown" href="settings.php?session=<?php echo $_SESSION['username']?>">
                <i class="fas fa-cogs fa-sm fa-fw mr-2"></i>
                Paramètres
            </a>
            <?php
                }
            ?>

            <!-- Diviseur -->
            <div class="dropdown-divider"></div>
            
            <!-- Nouveau choix -->
            <a class="dropdown-item red-hover" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2"></i>
                Déconnexion
            </a>
        </div>
    </li>
</ul>
</nav>
<!-- Fin de la topbar -->