<!-- Barre de navigation -->
<ul class="navbar-nav bg-dark sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Logo de la navbar -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php?session=<?php echo $_SESSION['username'] ?>">
        <!-- Icône -->
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-cogs"></i>
        </div>
        <!-- Texte -->
        <div class="sidebar-brand-text mx-3">Brilliant <sub>Panel</sub></div>
    </a>

    <!-- Diviseur -->
    <hr class="sidebar-divider my-0">

    <!-- Onglet - Dashboard -->
    <!-- TODO : Changer le style du lien pour montrer sa localisation sur le site -->
    <li class="nav-item">
        <a class="nav-link" href="index.php?session=<?php echo $_SESSION['username'] ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Tableau de bord</span>
        </a>
    </li>

    <!-- Diviseur -->
    <hr class="sidebar-divider">

    <?php 
        if ($_SESSION['perm'] == 3) {
    ?>
    <!-- Titre de section -->
    <div class="sidebar-heading">
        Supervision
    </div>

    <!-- Onglet - Logs -->
    <li class="nav-item">
        <a class="nav-link" href="logs.php?session=<?php echo $_SESSION['username'] ?>">
            <i class="fas fa-newspaper"></i>
            <span>Logs</span>
        </a>
    </li>

    <!-- Onglet - Settings -->
    <li class="nav-item">
        <a class="nav-link" href="settings.php?session=<?php echo $_SESSION['username'] ?>">
            <i class="fas fa-sliders-h"></i>
            <span>Paramètres</span>
        </a>
    </li>

    <!-- Onglet - Statistiques -->
    <li class="nav-item">
        <a class="nav-link" href="stats.php?session=<?php echo $_SESSION['username'] ?>">
            <i class="fas fa-chart-line"></i>
            <span>Statistiques</span>
        </a>
    </li>

    <!-- Diviseur -->
    <hr class="sidebar-divider">

    <?php 
        }
    ?>

    <!-- Titre de section -->
    <div class="sidebar-heading">
        Gestion
    </div>

    <?php 
        if ($_SESSION['perm'] >= 2) {
    ?>

    <!-- Onglet - Autres -->
    <li class="nav-item">
        <a class="nav-link" href="requests.php?session=<?php echo $_SESSION['username'] ?>">
            <i class="fas fa-comments"></i>
            <span>Requêtes</span>
        </a>
    </li>

    <!-- Onglet - Utilisateurs -->
    <li class="nav-item">
        <a class="nav-link" href="users.php?session=<?php echo $_SESSION['username'] ?>">
            <i class="fas fa-users"></i>
            <span>Utilisateurs</span>
        </a>
    </li>

    <?php 
        }
    ?>

    <!-- Onglet - Contacts -->
    <li class="nav-item">
        <a class="nav-link" href="contacts.php?session=<?php echo $_SESSION['username'] ?>">
            <i class="fas fa-phone-alt"></i>
            <span>Base de contacts</span>
        </a>
    </li>

    <!-- Diviseur -->
    <hr class="sidebar-divider">

    <!-- Activation / Désactivation de la navbar -->
    <!-- TODO : Sauvegarder l'état de la navbar -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle" aria-label="Etendre la barre de navigation"></button>
    </div>
</ul>
<!-- Fin de la barre de navigation -->