<!-- Footer -->
<footer class="sticky-footer bg-white" id="footerBp">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span><b>Copyright &copy; Brilliant<sup>P</sup> <?php $curDate = date('Y'); if ($curDate == '2021') { echo date('Y'); } else { echo '2021-'.date('Y');} ?> | Made with &hearts; by <a href="https://github.com/dotCore-off" target="_blank" rel="noopener">dotCore</a></b></span>
        </div>
    </div>
</footer>
<!-- Fin du footer -->