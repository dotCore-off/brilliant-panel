<?php

// On récupère le fichier distant dans un premier temps
$contents = file_get_contents('https://www.brilliant-lab.xyz/www.brilliant-lab.xyz/files.json');
$localContent = file_get_contents('files.json');

// Dans le cas où il n'y a pas de mise à jour
if (!$contents or strlen($contents) < 5) {
    // On quitte le script et on retourne sur la page des paramètres
    header('Location: ../settings.php?session='.$_SESSION['username'].'&code=0');
    exit();
}

// On vérifie le contenu et la présence du fichier en local
if (!empty($localContent)) {
    // Si jamais les deux contenus sont les mêmes, ça veut dire que le panel est déjà en dernière version
    if ($contents == $localContent) {
        // On quitte le script et on retourne sur la page des paramètres
        header('Location: ../settings.php?session='.$_SESSION['username'].'&code=0');
        exit();
    }
}

// Si tout est bon, on remplit le fichier local et on redirige sur la page des paramètres
file_put_contents("files.json", $contents);
header('Location: ../settings.php?session='.$_SESSION['username'].'&code=1');