<?php

const BUFFER = 1024;

function download($remoteFile, $localFile): bool {
    $fremote = fopen($remoteFile, 'rb');
    if (!$fremote) {
        header('Location: ../stats.php?session='.$_SESSION['username']);
        return false;
    }

    $flocal = fopen($localFile, 'wb');
    if (!$flocal) {
        fclose($fremote);
        header('Location: ../stats.php?session='.$_SESSION['username']);
        return false;
    }

    while ($buffer = fread($fremote, BUFFER)) {
        fwrite($flocal, $buffer);
    }

    fclose($flocal);
    fclose($fremote);

    header('Location: ../settings.php?session='.$_SESSION['username'].'');

    return true;
}

download('https://getbp.dotcore-lab.net/test.json', 'files.json');