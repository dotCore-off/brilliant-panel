<?php
// Intégration de la config
include_once('configuration/db.php');

// Si session invalide > Login
if (!isset($_SESSION['username'])) {
    header('Location: login.php');
}

// Si session valide > Connexion
elseif ((!isset($_GET['session'])) and (isset($_SESSION['username']))) {
    header('Location: settings.php?session='.$_SESSION['username'].'');
}
?>

<!DOCTYPE html>
<html lang="fr">
<!--
    Système créé par Alexis 'dotCore' Badel pour Habitat Métropole
            Reproduction complète ou partielle interdite
                    Tous droits réservés 2021
-->
<head>
    <!-- Elements de base -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Page affichant les différentes statistiques du panel">
    <meta name="author" content="Alexis 'dotCore' Badel">
    <title>BP - Statistiques</title>

    <!-- Importation des polices d'écriture -->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Importation des fichiers de style -->
    <link href="assets/css/style.min.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/avatar_user.png">

</head>

<body id="page-top">

    <!-- Wrapper de la page -->
    <div id="wrapper">

        <!-- Intégration - Navbar -->
        <?php include("elements/navbar.php") ?>

        <!-- Deuxième wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Contenu principal -->
            <div id="content">

                <!-- Intégration - Topbar -->
                <?php include("elements/topbar.php") ?>

                <!-- Intégration - Contenu -->
                <?php include("pages/stats_content.php") ?>

            </div>

            <!-- Intégration - Footer -->
            <?php include("elements/footer.php") ?>

        </div>
        <!-- Fin du wrapper du contenu -->

    </div>
    <!-- Fin du wrapper de la page -->

    <!-- Bouton - Revenir en haut de la page -->
    <a class="scroll-to-top rounded bg-primary" href="#page-top">
        <i class="fas fa-arrow-circle-up"></i>
    </a>

    <!-- Intégration - Formulaire de déconnexion -->
    <?php include("forms/form_deconnexion.php") ?>

    <!-- Intégration - Script de vérification d'environnement -->
    <?php include("scripts/check_env.php") ?>
    <?php include("scripts/notifications.php") ?>

    <!-- Scripts importants -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="assets/js/panel.min.js"></script>
    <script src="assets/js/tata/dist/tata.js"></script>
</body>
</html>