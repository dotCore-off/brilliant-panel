<!-- Formulaire d'ajout d'un contact -->
<div class="modal fade" id="addContact" aria-hidden="true" aria-labelledby="..." tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <!-- Header du formulaire -->
            <div class="modal-header">
                <!-- Titre -->
                <h1 class="h4 text-gray-900 mb-4">Ajouter un contact</h1>
                <!-- Croix pour fermer -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!-- Corps du formulaire -->
            <div class="modal-body">
                <!-- Formulaire principal -->
                <form class="user" id="addContactForm" method="post" action="scripts/add_contact.php">
                    <!-- Nom -->
                    <label for="newContactNom" class="form-label text-gray-800"><b>Nom du contact</b></label>
                    <div class="form-group input-group">
                        <input type="text" name="addContactName" class="form-control form-control-user mt-2" id="newContactNom" placeholder="Entrez le nom du contact" onkeyup="refreshContent()">
                    </div>
                    <!-- Prénom -->
                    <label for="newContactPrenom" class="form-label text-gray-800"><b>Prénom du contact</b></label>
                    <div class="form-group input-group">
                        <input type="text" name="addContactUser" class="form-control form-control-user mt-2" id="newContactPrenom" placeholder="Entrez le prénom du contact" onkeyup="refreshContent()">
                    </div>
                    <!-- Numéro interne -->
                    <label for="newContactNumint" class="form-label text-gray-800"><b>Numéro interne</b></label>
                    <div class="form-group input-group"> 
                        <input type="number" name="addContactNumint" class="form-control form-control-user mt-2" id="newContactNumint" placeholder="Entrez le numéro interne (si existant)" onkeyup="refreshContent()">
                    </div>               
                    <!-- Numéro externe -->
                    <label for="newContactNumext" class="form-label text-gray-800"><b>Numéro externe</b></label>
                    <div class="form-group">
                        <input type="tel" name="addContactNumext" class="form-control form-control-user mt-2" id="newContactNumext" placeholder="Entrez le numéro externe (si existant)" onkeyup="refreshContent()">
                    </div>
                    <!-- Commentaire -->
                    <label for="newContactNote" class="form-label text-gray-800"><b>Commentaire(s)</b></label>
                    <div class="form-group input-group"> 
                        <input type="text" name="addContactNote" class="form-control form-control-user mt-2" id="newContactNote" placeholder="Note(s) concernant le contact (optionnel)" onkeyup="refreshContent()">
                    </div> 
                </form> 
            </div>
                
            <!-- Footer du formulaire -->
            <div class="modal-footer">
                <!-- Bouton d'annulation -->
                <button type="button" class="btn btn-secondary btn-icon-split align-baseline" data-dismiss="modal" onClick="cancelNotif()">
                    <!-- Icône -->
                    <span class="icon text-white-75">
                        <i class="fas fa-times-circle"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Annuler</span>
                </button>

                <!-- Bouton de validation -->
                <button type="submit" form="addContactForm" class="btn btn-success btn-icon-split" id="confirmContactBtn">
                    <!-- Icône FA -->
                    <span class="icon text-white-75">
                        <i class="fas fa-plus"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Ajouter le contact</span>
                </button>                    
            </div>
        </div>
    </div>
</div>