<!-- Formulaire d'entrée du mail -->
<div class="modal fade" id="trunkModalTwo" aria-hidden="true" aria-labelledby="..." tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <!-- Header du formulaire -->
            <div class="modal-header">
                <!-- Titre -->
                <h1 class="h4 text-gray-900 mb-4">Utilisateur par défaut</h1>
                <!-- Croix pour fermer -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!-- Corps du formulaire -->
            <div class="modal-body">
                <p class="text text-gray-800">
                    Avant de vider la table, vous devez obligatoirement créer un utilisateur par défaut, sans quoi, vous ne pourrez plus avoir accès au panel.
                </p>
                <!-- Formulaire de connexion principal - TODO : Ajout de labels sur les inputs -->
                <form class="user" id="trunkUserForm" method="post" action="scripts/trunk_users.php?session=<?php echo $_SESSION['username'] ?>">
                    <!-- Adresse mail -->
                    <span class="text text-gray-800"><b>Adresse mail</b></span>
                    <div class="form-group input-group"> 
                        <input type="text" name="addMail2" class="form-control form-control-user mt-2" id="exampleInputEmail" placeholder="Entrez l'adresse mail">
                    </div>
                    <!-- Nom d'utilisateur -->
                    <span class="text text-gray-800"><b>Nom d'utilisateur</b></span>
                    <div class="form-group input-group"> 
                        <input type="text" name="addUser2" class="form-control form-control-user mt-2" id="exampleInputEmail" placeholder="Entrez le nom d'utilisateur">
                    </div>               
                    <!-- Mot de passe -->
                    <span class="text text-gray-800"><b>Mot de passe</b></span>
                    <div class="form-group">
                        <input type="password" name="addPass2" class="form-control form-control-user mt-2" id="exampleInputPassword" placeholder="Entrez le mot de passe">
                    </div>
                    <!-- Avatar -->
                    <span class="text text-gray-800"><b>Privilèges</b></span>
                    <div class="form-group input-group"> 
                        <input type="text" name="addLevel2" class="form-control form-control-user mt-2" id="exampleInputEmail" placeholder="1 = Utilisateur | 2 = Admin | 3 = Superadmin">
                    </div> 
                </form> 
            </div>
                
            <!-- Footer du formulaire -->
            <div class="modal-footer">
                <!-- Bouton de demande d'un opérateur -->
                <button type="button" class="btn btn-secondary btn-icon-split align-baseline" data-dismiss="modal" onClick="cancelNotif()">
                    <!-- Icône -->
                    <span class="icon text-white-75">
                        <i class="fas fa-times-circle"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Annuler</span>
                </button>

                <!-- Bouton d'ouverture d'un nouveau form -->
                <button type="submit" form="trunkUserForm" class="btn btn-success btn-icon-split" onClick="trunkNotif()">
                    <!-- Icône FA -->
                    <span class="icon text-white-75">
                        <i class="fas fa-plus"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Ajouter</span>
                </button>                 
            </div>
        </div>
    </div>
</div>