<!-- Formulaire de déconnexion -->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- En-tête -->
            <div class="modal-header">
                <!-- Titre -->
                <h1 class="h4 text-gray-900 mb-4">Êtes-vous sûr, <?php echo $_SESSION['username'] ?> ?</h1>

                <!-- Croix de fermeture -->
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!-- Corps -->
            <div class="modal-body">
                Cela mettra fin à la session en cours et vous retournerez immédiatement à l'écran de connexion.<br><strong>Pensez à bien sauvegarder les changements que vous avez pu effectué pour ne rien perdre.</strong>
            </div>
            <!-- Footer -->
            <div class="modal-footer">
                <!-- Bouton d'annulation' -->
                <button type="button" class="btn btn-secondary btn-icon-split align-baseline" data-dismiss="modal">
                    <!-- Icône -->
                    <span class="icon text-white-75">
                        <i class="fas fa-times-circle"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Annuler</span>
                </button>

                <!-- Bouton de déconnexion -->
                <a href="scripts/disconnect.php" class="btn btn-danger btn-icon-split">
                    <!-- Icône FA -->
                    <span class="icon text-white-75">
                        <i class="fas fa-sign-out-alt"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Déconnexion</span>
                </a> 
            </div>
        </div>
    </div>
</div>
<!-- Fin du formulaire de déconnexion -->
