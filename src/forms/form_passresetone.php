<!-- Formulaire de réinitialisation de mot de passe -->
<div class="modal fade" id="firstForm" aria-hidden="true" aria-labelledby="..." tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <!-- Header du formulaire -->
            <div class="modal-header">
                <!-- Titre -->
                <h1 class="h4 text-gray-900 mb-3">Récupération de mot de passe</h1>
                <!-- Croix pour fermer -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!-- Corps du formulaire -->
            <div class="modal-body">
                <p class="text text-gray-900">Veuillez sélectionner une des options ci-dessous, soit :
                    <br>
                        - une <strong>aide en direct</strong> vous est fournie
                    <br>
                        - une <strong>demande est envoyée au service informatique</strong>
                </p>
            </div>

            <!-- Footer du formulaire -->
            <div class="modal-footer">
                <!-- Bouton de demande d'un opérateur -->
                <a href="#" class="btn btn-primary btn-icon-split mr-4" data-dismiss="modal" onClick="opeNotif()">
                    <!-- Icône -->
                    <span class="icon text-white-75">
                        <i class="fas fa-headset"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Aide d'un opérateur</span>
                </a>

                <!-- Bouton d'ouverture d'un nouveau form -->
                <button type="button" class="btn btn-danger btn-icon-split ml-4" data-target="#mdpForm" data-toggle="modal" data-dismiss="modal">
                    <!-- Icône FA -->
                    <span class="icon text-white-75">
                        <i class="fas fa-history"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Faire une demande</span>
                </button>                    
            </div>
        </div>
    </div>
</div>