<!-- Formulaire d'entrée du mail -->
<div class="modal fade" id="addModal" aria-hidden="true" aria-labelledby="..." tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <!-- Header du formulaire -->
            <div class="modal-header">
                <!-- Titre -->
                <h1 class="h4 text-gray-900 mb-4">Ajouter un utilisateur</h1>
                <!-- Croix pour fermer -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!-- Corps du formulaire -->
            <div class="modal-body">
                <!-- Formulaire de connexion principal -->
                <form class="user" id="addUserForm" method="post" action="scripts/add_user.php">
                    <!-- Adresse mail -->
                    <label for="newUserMail" class="form-label text-gray-800"><b>Adresse mail</b></label>
                    <div class="form-group input-group">
                        <input type="email" name="addMail" class="form-control form-control-user mt-2" id="newUserMail" placeholder="Entrez l'adresse mail" onkeyup="refreshContent()">
                    </div>
                    <!-- Nom d'utilisateur -->
                    <label for="newUserName" class="form-label text-gray-800"><b>Nom d'utilisateur</b></label>
                    <div class="form-group input-group"> 
                        <input type="text" name="addUser" class="form-control form-control-user mt-2" id="newUserName" placeholder="Entrez le nom d'utilisateur" onkeyup="refreshContent()">
                    </div>               
                    <!-- Mot de passe -->
                    <label for="newUserPass" class="form-label text-gray-800"><b>Mot de passe</b></label>
                    <div class="form-group">
                        <input type="password" name="addPass" class="form-control form-control-user mt-2" id="newUserPass" placeholder="Entrez le mot de passe" onkeyup="refreshContent()">
                    </div>
                    <!-- Privilèges -->
                    <label for="newUserPerm" class="form-label text-gray-800"><b>Permissions</b></label>
                    <div class="form-group input-group">
                        <input type="number" name="addLevel" class="form-control form-control-user mt-2" id="newUserPerm" placeholder="1 = Utilisateur | 2 = Admin | 3 = Superadmin" onkeyup="refreshContent()">
                    </div> 
                </form> 
            </div>
                
            <!-- Footer du formulaire -->
            <div class="modal-footer">
                <!-- Bouton d'annulation -->
                <button type="button" class="btn btn-secondary btn-icon-split align-baseline" data-dismiss="modal" onClick="cancelNotif()">
                    <!-- Icône -->
                    <span class="icon text-white-75">
                        <i class="fas fa-times-circle"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Annuler</span>
                </button>

                <!-- Bouton de validation -->
                <button type="submit" form="addUserForm" class="btn btn-success btn-icon-split" id="confirmUserBtn">
                    <!-- Icône FA -->
                    <span class="icon text-white-75">
                        <i class="fas fa-plus"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Ajouter l'utilisateur</span>
                </button>                    
            </div>
        </div>
    </div>
</div>