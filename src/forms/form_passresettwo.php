<!-- Formulaire d'entrée du mail -->
<div class="modal fade" id="mdpForm" aria-hidden="true" aria-labelledby="..." tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <!-- Header du formulaire -->
            <div class="modal-header">
                <!-- Titre -->
                <h1 class="h4 text-gray-900 mb-4">Demande de réinitialisation</h1>
                <!-- Croix pour fermer -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!-- Corps du formulaire -->
            <form class="user" id="sendRequest" method="post" action="scripts/add_request.php">
                <div class="modal-body">
                    <p class="text text-gray-900">
                        <strong>Une demande sera envoyée</strong> et les opérateurs ayant accès au panel s'en occuperont <strong>dès que possible</strong>.<br>
                        <strong>Vous serez notifié par mail</strong> à l'adresse correspondant à l'utilisateur de la réponse du support.
                    </p>
                    <hr>
                    <!-- Adresse mail input -->
                    <div class="form-group">
                        <label for="userConcerned" class="form-label text-gray-800"><b>Adresse mail</b></label>
                        <input type="email" name="userConcerned" class="form-control form-control-user" id="userConcerned" aria-describedby="nomRequis" placeholder="Veuillez entrez l'adresse mail du compte concerné" onkeyup="refreshContent()">
                    </div>
                    <p class="text text-danger">
                        <sub>
                            <strong>
                                Notez que cela peut prendre un certain temps.
                            </strong>
                        </sub>
                    </p>
                </div>
            </form>
                
            <!-- Footer du formulaire -->
            <div class="modal-footer">
                <!-- Bouton de demande d'un opérateur -->
                <button type="button" class="btn btn-danger btn-icon-split align-baseline" data-target="#mdpForm" data-toggle="modal" onClick="cancelNotif()">
                    <!-- Icône -->
                    <span class="icon text-white-75">
                        <i class="fas fa-times-circle"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Annuler</span>
                </button>

                <!-- Bouton d'ouverture d'un nouveau form -->
                <button type="submit" form="sendRequest" class="btn btn-success btn-icon-split" id="sendResetBtn">
                    <!-- Icône FA -->
                    <span class="icon text-white-75">
                        <i class="fas fa-paper-plane"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Envoyer</span>
                </button>                    
            </div>
        </div>
    </div>
</div>