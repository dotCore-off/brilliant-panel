<!-- Formulaire d'entrée du mail -->
<div class="modal fade" id="trunkModalThree" aria-hidden="true" aria-labelledby="..." tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <!-- Header du formulaire -->
            <div class="modal-header">
                <!-- Titre -->
                <h1 class="h4 text-gray-900 mb-4">Vider la table ?</h1>
                <!-- Croix pour fermer -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!-- Corps du formulaire -->
            <div class="modal-body">
                <p class="text text-gray-800">
                    Êtes-vous sûr de vouloir vider la table des requêtes ? <br>
                </p>
                <p class="text text-danger"><strong>
                    Notez que cette action est irréversible et les données seront définitivement perdues.
                </strong></p>
            </div>
                
            <!-- Footer du formulaire -->
            <div class="modal-footer">
                <!-- Bouton de demande d'un opérateur -->
                <button type="button" class="btn btn-secondary btn-icon-split align-baseline" data-dismiss="modal" onClick="cancelNotif()">
                    <!-- Icône -->
                    <span class="icon text-white-75">
                        <i class="fas fa-times-circle"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Annuler</span>
                </button>

                <!-- Bouton de confirmation -->
                <a href="scripts/trunk_requests.php?session=<?php echo $_SESSION['username'] ?>" type="button" class="btn btn-danger btn-icon-split">
                    <!-- Icône FA -->
                    <span class="icon text-white-75">
                        <i class="fas fa-check-circle"></i>
                    </span>
                    <!-- Texte -->
                    <span class="text">Confirmer</span>
                </a>                    
            </div>
        </div>
    </div>
</div>