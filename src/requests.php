<?php 
    // Intégration de la config
    include_once('configuration/db.php');

    // Vérification de la session + URL
    if (!isset($_SESSION['username'])) {
        header('Location: login.php');
    }
    // Implémentation de la vérification de droits d'accès
    elseif (isset($_SESSION['username']) and $_SESSION['perm'] < 2) {
        header('Location: index.php?session='.$_SESSION['username'].'');
    }
    elseif ((!isset($_GET['session'])) and (isset($_SESSION['username']))) {
        header('Location: requests.php?session='.$_SESSION['username'].'');
    }
?>

<!DOCTYPE html>
<html lang="fr">
<!-- 
    Système créé par Alexis 'dotCore' Badel pour Habitat Métropole
            Reproduction complète ou partielle interdite
                    Tous droits réservés 2021
-->
<head>
    <!-- Elements de base -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Page de gestion des requêtes du panel d'administration">
    <meta name="author" content="Alexis 'dotCore' Badel">
    <title>BP - Demandes</title>

    <!-- Importation des polices d'écriture -->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Importation des fichiers de style -->
    <link href="assets/css/style.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/js/datables/datatables.min.css"/>

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/avatar_user.png">

</head>

<body id="page-top">

    <!-- Wrapper de la page -->
    <div id="wrapper">

        <!-- Intégration - Navbar -->
        <?php include("elements/navbar.php") ?>

        <!-- Deuxième wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Contenu principal -->
            <div id="content">

                <!-- Intégration - Topbar -->
                <?php include("elements/topbar.php") ?>
                
                <!-- Intégration - Contenu -->
                <?php include("pages/requests_content.php") ?>
                
            </div>

            <!-- Intégration - Footer -->
            <?php include("elements/footer.php") ?> 

        </div>
        <!-- Fin du wrapper du contenu -->

    </div>
    <!-- Fin du wrapper de la page -->

    <!-- Bouton - Revenir en haut de la page-->
    <a class="scroll-to-top rounded bg-primary" href="#page-top">
        <i class="fas fa-arrow-circle-up"></i>
    </a>

    <!-- Intégration - Formulaire de déconnexion -->
    <?php include("forms/form_deconnexion.php") ?> 
    <?php include("forms/form_trunktablethree.php") ?>

    <!-- Intégration - Script de vérification d'environnement -->
    <?php include("scripts/check_env.php") ?>
    <?php include("scripts/notifications.php") ?>

    <!-- Scripts importants -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="assets/js/panel.min.js"></script>
    <script src="assets/js/tata/dist/tata.js"></script>
    <script type="text/javascript" src="assets/js/datables/datatables.min.js"></script>

    <!-- Activation du tableau via DataTables -->
    <script>
        $(document).ready( function () {
            $('#requestsDBTable').dataTable( {
                // On désactive certaines fonctionnalités inutiles
                "ordering": false,
                "info": false,
                "paging": false,

                // Traduction en français des éléments du tableau
                language: {
                    processing:     "Traitement en cours...",
                    search:         "Rechercher&nbsp;:",
                    lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                }
            });
        });
    </script>

    <!-- Activation des Tooltips sur cette page -->
    <script>
        $(function () {
             $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</body>
</html>