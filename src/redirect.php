<?php

// Démarrage du système de sessions
session_start();

// Intégration des infos de BDD
include_once('dev/config.php');

// Si on a une session valide + bonne URL
if (isset($_GET['session']) and (isset($_SESSION['username']))) {
    // Redirection sur le panel
    header('Location: index.php?session='.$_SESSION['username'].'');
}

// Si on a une session valide + mauvaise URL
elseif (!isset($_GET['session']) and (isset($_SESSION['username']))) {
    // Redirection sur le panel
    header('Location: index.php?session='.$_SESSION['username'].'');
}

// Si on a une session invalide + bonne URL
elseif (isset($_GET['session']) and (!isset($_SESSION['username']))) {
    // Redirection sur le login
    header('Location: login.php');
}

else {
    // Sinon, redirection sur le login
    header('Location: login.php');
}

?>