<?php

// On inclut la configuration
include_once("configuration/db.php");

/*
 * Création de la table des utilisateurs
 */

$createUsers = "CREATE TABLE acp_users (username TEXT, mail TEXT, password TEXT, niveau INT)";
$creationQuery = $bdd->prepare($createUsers);
$creationQuery->execute();

/*
 *          Ici, on ajoute un utilisateur par défaut pour que
 * les gens puissent se connecter via au moins un utilisateur, Administrateur
 */

$addDefaultUser = "INSERT INTO acp_users (username, mail, password, niveau) VALUES(?, ?, ?, ?)";
$stmt = $bdd->prepare($addDefaultUser);
$stmt->execute(["Admin", "admin@bp.com", "Admin12345", "3"]);

/*
 * Création de la table des requêtes
 */

$createRequests = "CREATE TABLE acp_requests (email TEXT, date DATE, traitee INT)";
$requestQuery = $bdd->prepare($createRequests);
$requestQuery->execute();

/*
 * Création de la table des paramètres
 */

$createSettings = "CREATE TABLE acp_settings (motd_content TEXT, color_primary TEXT, alert TEXT)";
$settingsQuery = $bdd->prepare($createSettings);
$settingsQuery->execute();

/*
 *          Ici, on ajoute le MOTD par défaut pour que
 * l'on évite d'être bloqué dans le futur puisque l'on utilise UPDATE et non pas INSERT
 */

$defaultMotd = "INSERT INTO acp_settings (motd_content) VALUES (?)";
$motdQuery = $bdd->prepare($defaultMotd);
$motdQuery->execute(["<b>Ceci est le <u>message du jour</u>.</b><br>On peut y faire plein de choses, comme afficher <b><u>certaines informations importantes</u></b>.<br>Il supporte aussi le balisage en HTML.<br><br><i>Et ça, c'est cool !</i>"]);


/*
 * Création de la table des logs
 */

$createLogs = "CREATE TABLE acp_logs (user TEXT, event TEXT, date DATE, gravity INT)";
$logsQuery = $bdd->prepare($createLogs);
$logsQuery->execute();

/*
 * Création de la table des contacts
 */

$createContact = "CREATE TABLE acp_contacts (nom TEXT, prenom TEXT, numero_int TEXT, numero_ext TEXT, note TEXT)";
$contactQuery = $bdd->prepare($createContact);
$contactQuery->execute();

// Redirection sur le login
header('Location: login.php');