<!-- Contenu de la page -->
<div class="container-fluid">
    <!-- En-tête de page -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <!-- Titre de la page -->
        <h1 class="h3 mb-0 text-gray-800 ml-auto mr-auto">Gestion - Contacts</h1>
    </div>

    <!-- Récupération des utilisateurs en base de données -->
    <?php
    // Début - Requête
    $requete = $bdd->query('SELECT DISTINCT nom, prenom, numero_int, numero_ext, note FROM acp_contacts');
    $lignes = $requete->fetchAll(PDO::FETCH_ASSOC);

    // Fin - Requête
    $requete->closeCursor();

    // Définit de count
    $count = 0;
    ?>

    <!-- Création du tableau -->
    <table id="contactsDBTable" class="table table-bordered table-striped display">
        <!-- Header du tableau -->
        <thead>
        <!-- Colonnes -->
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Numéro interne</th>
            <th>Numéro externe</th>
            <th>Commentaire</th>
            <th>Actions</th>
        </tr>
        </thead>

        <!-- Corps du tableau -->
        <tbody>

        <!-- Remplissage du tableau-->
        <?php
        // Boucle sur les colonnes
        foreach($lignes as $ligne) {
            ?>
            <tr>
                <?php
                // Boucle sur les entrées
                foreach($ligne as $valeur) {
                    ?>
                    <!-- Nouvelle case + Valeur -->
                    <td>
                        <?php
                        echo $valeur;
                        ?>
                    </td>
                    <?php
                }
                ?>
                <?php
                /* Ici, on va définir deux variables :
                *       - la première va permettre de stocker une ligne entière
                *           qui contient donc toutes les infos d'un utilisateur
                *
                *       - la deuxième instruction va permettre d'itérer pour
                *           tout simplement effectuer un changement de lien
                *
                *  Ainsi, on commence par la ligne 0 (un tableau s'itère à 0) et on enchaîne
                */
                $arrayToVar = $lignes[$count];
                $count = $count + 1;
                ?>
                <td>
                    <!-- Bouton pour modifier -->
                    <a href="edit.php?session=<?php echo $_SESSION['username'] ?>&nom=<?php echo $arrayToVar['nom'] ?>&prenom=<?php echo $arrayToVar['prenom'] ?><?php if (isset($arrayToVar['numero_int'])) { ?>&numero_int=<?php echo $arrayToVar['numero_int'] ?><?php }?><?php if (isset($arrayToVar['numero_ext'])) { ?>&numero_ext=<?php echo $arrayToVar['numero_ext'] ?><?php }?><?php if (isset($arrayToVar['note'])) { ?>&note=<?php echo $arrayToVar['note'] ?><?php }?>>" class="btn btn-outline-info shadow mr-2" data-toggle="tooltip" data-placement="top" title="Modifier">
                        <i class="far fa-edit"></i>
                    </a>

                    <!-- Bouton pour supprimer -->
                    <a href="scripts/remove.php?session=<?php echo $_SESSION['username'] ?>&nom=<?php echo $arrayToVar['nom'] ?>&prenom=<?php echo $arrayToVar['prenom'] ?><?php if (isset($arrayToVar['numero_int'])) { ?>&numero_int=<?php echo $arrayToVar['numero_int'] ?><?php }?><?php if (isset($arrayToVar['numero_ext'])) { ?>&numero_ext=<?php echo $arrayToVar['numero_ext'] ?><?php }?><?php if (isset($arrayToVar['note'])) { ?>&note=<?php echo $arrayToVar['note'] ?><?php }?>" class="btn btn-outline-danger shadow" data-toggle="tooltip" data-placement="top" title="Supprimer">
                        <i class="fas fa-trash"></i>
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>

    </table>
    <!-- Fin du tableau -->

    <!-- Bouton pour ajouter un utilisateur -->
    <button type="button" class="btn btn-outline-success shadow btnDoc mt-4 ml-auto mr-auto" data-toggle="modal" data-target="#addContact">
        <i class="fas fa-plus"></i>
        Ajouter un contact
    </button>

    <!-- Bouton pour vider la table -->
    <button type="button" class="btn btn-outline-info shadow btnDoc mt-4 ml-auto mr-auto" data-toggle="modal" data-target="#">
        <i class="fas fa-sync"></i>
        Synchroniser avec l'annuaire
    </button>
</div>