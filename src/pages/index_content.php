<!-- Contenu de la page -->
<div class="container-fluid" id="backgroundBp">
    <!-- En-tête de page -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <!-- Titre de la page -->
        <h1 class="h3 mb-0 text-gray-800 text-center ml-auto mr-auto">Bienvenue sur le panel, <?php echo $_SESSION['username'] ?> !</h1>
    </div>

    <!-- Première ligne de contenu -->
    <div class="row">
        <!-- Carte de présentation -->
        <div class="col-xl-5 col-md-6 mb-4 ml-lg-auto mr-lg-auto">
            <!-- Description -->
            <div class="card border-bottom-primary shadow mb-4 position-sticky">
                <div class="d-block card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Bienvenue sur le Brilliant Panel !</h6>
                </div>
                <div class="collapse show">
                    <div class="card-body">
                        <p class="mt-0 mb-2 text-gray-800"><strong>Brilliant<sup>Panel</sup></strong> est une interface web <b>flexible et intelligente</b>
                            permettant de gérer efficacement l'ensemble de votre parc téléphonique,
                            tout en implémentant de nombreuses fonctionnalités externes.
                        </p>
                        <p class="mb-2 text-gray-800"><b>Légère, claire et modulable</b>, Brilliant<sup>P</sup> permet entre autre
                            l'intégration d'autres éléments grâce à son <b>code simple et expliqué</b>
                            ainsi que ses documentations techniques <b>compréhensibles et pensées pour tous</b>.<br>
                        </p>
                        <p class="mb-1 text-gray-800">
                            <strong>En vous souhaitant une agréable navigation !</strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php
        // On exécute une requête pour récupérer le contenu de la colonne motd_content
        $motdQuery = $bdd->query('SELECT DISTINCT motd_content from acp_settings');

        // On récupère le résultat de la requête ci-dessus
        $results = $motdQuery->fetch(PDO::FETCH_ASSOC);

        // On met fin à la requête pour libérer de la mémoire
        $motdQuery->closeCursor();

        $motdFinalContent = "";

        // On boucle à travers les résultats pour éviter d'avoir une erreur Array => string conversion
        foreach ($results as $message) {
            // On stock dans la variable
            $motdFinalContent = $message;
        }
        if (isset($motdFinalContent) and strlen($motdFinalContent) > 0) {
        ?>
        <!-- Carte du MOTD -->
        <div class="col-xl-5 col-md-6 mb-4 ml-lg-auto mr-lg-auto">
            <!-- Description -->
            <div class="card border-bottom-primary shadow mb-4 position-sticky">
                <div class="d-block card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">[<?php $dateOfTheDay = date("d-m-Y"); echo $dateOfTheDay ?>] Message du jour</h6>
                </div>
                <div class="collapse show">
                    <div class="card-body text-gray-900">
                        <?php
                        echo $motdFinalContent;
                        ?>
                    </div>
                </div>
            </div>
        </div>
            <?php
        }
        ?>
    </div>
    <div class="row">
        <?php
        if ($_SESSION['perm'] < 3) {
        ?>
        <!-- Carte - Contacts -->
        <div class="col-xl-3 col-md-6 mb-4 mr-auto ml-auto">
            <!-- Bordures -->
            <a href="contacts.php?session=<?php echo $_SESSION['username'] ?>" class="card lift border-left-dark border-top-dark border-bottom-dark border-right-dark shadow h-100 py-2">
                <!-- Contenu de la carte -->
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <!-- Textes -->
                        <div class="col mr-2">
                            <!-- Titre -->
                            <div class="text-xs font-weight-bold text-dark text-uppercase mb-1 text-center">Brilliant Panel - Gestion</div>
                            <!-- Contenu principal -->
                            <div class="h5 mb-0 font-weight-bold text-primary text-center">Gérer les contacts</div>
                        </div>
                        <!-- Icône - Droite -->
                        <div class="col-auto">
                            <i class="fas fa-phone-alt fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <?php
        }
        ?>
        <?php
        if ($_SESSION['perm'] == 2) {
        ?>
        <!-- Carte - Utilisateurs -->
        <div class="col-xl-3 col-md-6 mb-4 mr-auto ml-auto">
            <!-- Bordures -->
            <a href="users.php?session=<?php echo $_SESSION['username'] ?>" class="card lift border-left-dark border-top-dark border-bottom-dark border-right-dark shadow h-100 py-2">
                <!-- Contenu de la carte -->
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <!-- Textes -->
                        <div class="col mr-2">
                            <!-- Titre -->
                            <div class="text-xs font-weight-bold text-dark text-uppercase mb-1 text-center">Brilliant Panel - Gestion</div>
                            <!-- Contenu principal -->
                            <div class="h5 mb-0 font-weight-bold text-primary text-center">Gérer les utilisateurs</div>
                        </div>
                        <!-- Icône - Droite -->
                        <div class="col-auto">
                            <i class="fas fa-users fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- Carte - Requêtes -->
        <div class="col-xl-3 col-md-6 mb-4 mr-auto ml-auto">
            <!-- Bordures -->
            <a href="requests.php?session=<?php echo $_SESSION['username'] ?>" class="card lift border-left-dark border-top-dark border-bottom-dark border-right-dark shadow h-100 py-2">
                <!-- Contenu de la carte -->
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <!-- Textes -->
                        <div class="col mr-2">
                            <!-- Titre -->
                            <div class="text-xs font-weight-bold text-dark text-uppercase mb-1 text-center">Brilliant Panel - Gestion</div>
                            <!-- Contenu principal -->
                            <div class="h5 mb-0 font-weight-bold text-primary text-center">Gérer les requêtes</div>
                        </div>
                        <!-- Icône - Droite -->
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <?php
        }
        ?>
        <?php
        if ($_SESSION['perm'] == 3) {
            ?>
            <!-- Carte - Logs -->
            <div class="col-xl-3 col-md-6 mb-4 mr-auto ml-auto">
                <!-- Bordures -->
                <a href="logs.php?session=<?php echo $_SESSION['username'] ?>" class="card lift border-left-dark border-top-dark border-bottom-dark border-right-dark shadow h-100 py-2">
                    <!-- Contenu de la carte -->
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <!-- Textes -->
                            <div class="col mr-2">
                                <!-- Titre -->
                                <div class="text-xs font-weight-bold text-dark text-uppercase mb-1 text-center">Brilliant Panel - Supervision</div>
                                <!-- Contenu principal -->
                                <div class="h5 mb-0 font-weight-bold text-primary text-center">Vérifier les logs</div>
                            </div>
                            <!-- Icône - Droite -->
                            <div class="col-auto">
                                <i class="fas fa-newspaper fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Carte - Paramètres -->
            <div class="col-xl-3 col-md-6 mb-4 mr-auto ml-auto">
                <!-- Bordures -->
                <a href="settings.php?session=<?php echo $_SESSION['username'] ?>" class="card lift border-left-dark border-top-dark border-bottom-dark border-right-dark shadow h-100 py-2">
                    <!-- Contenu de la carte -->
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <!-- Textes -->
                            <div class="col mr-2">
                                <!-- Titre -->
                                <div class="text-xs font-weight-bold text-dark text-uppercase mb-1 text-center">Brilliant Panel - Supervision</div>
                                <!-- Contenu principal -->
                                <div class="h5 mb-0 font-weight-bold text-primary text-center">Modifier les paramètres</div>
                            </div>
                            <!-- Icône - Droite -->
                            <div class="col-auto">
                                <i class="fas fa-cogs fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Carte - Statistiques -->
            <div class="col-xl-3 col-md-6 mb-4 mr-auto ml-auto">
                <!-- Bordures -->
                <a href="stats.php?session=<?php echo $_SESSION['username'] ?>" class="card lift border-left-dark border-top-dark border-bottom-dark border-right-dark shadow h-100 py-2">
                    <!-- Contenu de la carte -->
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <!-- Textes -->
                            <div class="col mr-2">
                                <!-- Titre -->
                                <div class="text-xs font-weight-bold text-dark text-uppercase mb-1 text-center">Brilliant Panel - Supervision</div>
                                <!-- Contenu principal -->
                                <div class="h5 mb-0 font-weight-bold text-primary text-center">Voir les statistiques</div>
                            </div>
                            <!-- Icône - Droite -->
                            <div class="col-auto">
                                <i class="fas fa-chart-line fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php
        }
        ?>
    </div>
    <!-- Troisième ligne de contenu -->
    <div class="row">
        <div class="col-xl-4 col-md-6 mb-4 mt-4 mr-auto ml-auto">
            <!-- Bordures -->
            <a href="https://github.com/dotCore-off/avaya-corepanel" class="card lift pnCard border-top-dark border-bottom-dark border-left-dark border-right-dark shadow h-100 py-2">
                <!-- Contenu de la carte -->
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <!-- Icône -->
                        <div class="col-auto">
                            <i class="fas fa-star fa-2x text-primary"></i>
                        </div>
                        <div class="col mr-2">
                            <!-- Titre -->
                            <div class="text-xs font-weight-bold text-dark text-uppercase mb-1 text-center">Nouvelle version disponible !</div>
                            <!-- Contenu principal -->
                            <div class="h6 mb-0 font-weight-bold text-gray-800 text-center">Version 0.4 - Control the Dark Side !</div>
                        </div>
                        <!-- Icône -->
                        <div class="col-auto">
                            <i class="fas fa-star fa-2x text-primary"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>