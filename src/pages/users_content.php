<!-- Contenu de la page -->
<div class="container-fluid">
    <!-- En-tête de page -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <!-- Titre de la page -->
        <h1 class="h3 mb-0 text-gray-800 ml-auto mr-auto">Gestion - Utilisateurs</h1>
    </div>

    <!-- Récupération des utilisateurs en base de données -->
    <?php
        // Début - Requête
        $requete = $bdd->query('SELECT DISTINCT username, mail, password, niveau FROM acp_users');
        $lignes = $requete->fetchAll(PDO::FETCH_ASSOC);

        // Fin - Requête
        $requete->closeCursor();

        // Définit de count
        $count = 0;
    ?>

    <!-- Création du tableau -->
    <table id="usersDBTable" class="table table-bordered table-striped display">
        <!-- Header du tableau -->
        <thead>
            <!-- Colonnes -->
            <tr>
                <th>Nom d'utilisateur</th>
                <th>Adresse mail</th>
                <th>Mot de passe</th>
                <th>Privilèges</th>
                <th>Actions</th>
            </tr>
        </thead>

        <!-- Corps du tableau -->
        <tbody>

        <!-- Remplissage du tableau-->
        <?php
            // Boucle sur les colonnes
            foreach($lignes as $ligne) {
        ?>
            <tr>
            <?php
                // Boucle sur les entrées
                foreach($ligne as $valeur) {
            ?> 
                <!-- Nouvelle case + Valeur -->
                <td>
                    <?php 
                        echo $valeur; 
                    ?>
                </td>
            <?php
                }
            ?>
            <?php 
                /* Ici, on va définir deux variables :
                *       - la première va permettre de stocker une ligne entière 
                *           qui contient donc toutes les infos d'un utilisateur
                *
                *       - la deuxième instruction va permettre d'itérer pour
                *           tout simplement effectuer un changement de lien
                *
                *  Ainsi, on commence par la ligne 0 (un tableau s'itère à 0) et on enchaîne
                */
                $arrayToVar = $lignes[$count];
                $count = $count + 1;
            ?>
                <td>
                    <!-- Bouton pour modifier -->
                    <a href="edit.php?session=<?php echo $_SESSION['username'] ?>&mail=<?php echo $arrayToVar['mail'] ?>&name=<?php echo $arrayToVar['username'] ?>&pass=<?php echo $arrayToVar['password'] ?>&priv=<?php echo $arrayToVar['niveau'] ?>" class="btn btn-outline-dark shadow mr-2" data-toggle="tooltip" data-placement="top" title="Modifier">
                        <i class="far fa-edit"></i>
                    </a>

                    <!-- Bouton pour dupliquer -->
                    <a href="scripts/paste.php?session=<?php echo $_SESSION['username'] ?>&name=<?php echo $arrayToVar['username'] ?>&mail=<?php echo $arrayToVar['mail'] ?>&pass=<?php echo $arrayToVar['password'] ?>&priv=<?php echo $arrayToVar['niveau'] ?>" class="btn btn-outline-primary shadow" data-toggle="tooltip" data-placement="top" title="Dupliquer">
                        <i class="fas fa-paste"></i>
                    </a>

                    <?php
                    if ($_SESSION['perm'] == 3) {
                    ?>
                    <!-- Bouton pour supprimer -->
                    <a href="scripts/remove.php?session=<?php echo $_SESSION['username'] ?>&name=<?php echo $arrayToVar['username'] ?>" class="btn btn-outline-danger shadow ml-2" data-toggle="tooltip" data-placement="top" title="Supprimer">
                        <i class="fas fa-trash"></i>
                    </a>
                    <?php
                    }
                    ?>
                </td>
            </tr>
        <?php
            }  
        ?>
        </tbody>

    </table>
    <!-- Fin du tableau -->

    <!-- Bouton pour ajouter un utilisateur -->
    <button type="button" class="btn btn-outline-success shadow btnDoc mt-4 ml-auto mr-auto" data-toggle="modal" data-target="#addModal">
        <i class="fas fa-plus"></i>
        <span class="txtBut">Ajouter</span>
    </button>

    <!-- Bouton pour vider la table -->
    <button type="button" class="btn btn-outline-danger shadow btnDoc mt-4 ml-auto mr-auto" data-toggle="modal" data-target="#trunkModalOne">
        <i class="fas fa-trash-alt"></i>
        <span class="txtBut">Vider la table</span>
    </button>
</div>