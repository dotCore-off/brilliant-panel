<!-- Contenu de la page -->
<div class="container-fluid">
    <!-- En-tête de page -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <!-- Titre de la page -->
        <h1 class="h3 mb-0 text-gray-800 ml-auto mr-auto">Gestion - Demandes</h1>
    </div>

    <!-- Récupération des utilisateurs en base de données -->
    <?php
        // Début - Requête
        $requete = $bdd->query('SELECT DISTINCT email, date, traitee FROM acp_requests');
        $lignes = $requete->fetchAll(PDO::FETCH_ASSOC);

        // Fin - Requête
        $requete->closeCursor();
    ?>

    <!-- Création du tableau -->
    <table id="requestsDBTable" class="table table-bordered table-striped display">
        <!-- Header du tableau -->
        <thead>
            <!-- Colonnes -->
            <tr>
                <th>Nom d'utilisateur</th>
                <th>Date de la demande</th>
                <th>État de la demande</th>
                <th>Actions</th>
            </tr>
        </thead>

        <!-- Corps du tableau -->
        <tbody>

        <!-- Remplissage du tableau-->
        <?php
            // Boucle sur les colonnes
            foreach($lignes as $ligne) {
        ?>
            <tr>
            <?php
                // Boucle sur les entrées
                foreach($ligne as $valeur) {
            ?> 
                <!-- Nouvelle case + Valeur -->
                <td>
                    <?php 
                        echo $valeur; 
                    ?>
                </td>
            <?php
                }
            ?>
                <td>
                    <!-- Bouton pour approuver + envoi d'un mail - TODO : Rendre fonctionnel -->
                    <a href="#" class="btn btn-outline-success shadow mr-2" data-toggle="tooltip" data-placement="top" title="Approuver">
                        <i class="fas fa-check"></i>
                    </a>

                    <!-- Bouton pour refuser + envoi d'un mail - TODO : Rendre fonctionnel -->
                    <a href="#" class="btn btn-outline-danger shadow" data-toggle="tooltip" data-placement="top" title="Refuser">
                        <i class="fas fa-times"></i>
                    </a>

                    <!-- Bouton pour contacter - TODO : Rendre fonctionnel -->
                    <a href="#" class="btn btn-outline-primary shadow ml-2" data-toggle="tooltip" data-placement="top" title="Contacter">
                        <i class="fas fa-headset"></i>
                    </a>
                </td>
            </tr>
        <?php
            }  
        ?>
        </tbody>

    </table>
    <!-- Fin du tableau -->

    <!-- Bouton pour vider la table -->
    <button type="button" class="btn btn-outline-danger shadow btnDoc mt-4" data-toggle="modal" data-target="#trunkModalThree">
        <i class="fas fa-trash-alt"></i>
        <span class="txtBut">Supprimer toutes les demandes</span>
    </button>
</div>