<?php
    // Si la session n'est pas valide
    if (!isset($_SESSION['username'])) {
        header('Location: login.php');
    }

    // On vérifie que les données soient bonnes
    if ((!empty($_GET['mail']) and !empty($_GET['name']) and !empty($_GET['pass']) and !empty($_GET['priv'])) or (!empty($_GET['nom']) and !empty($_GET['prenom']) and (isset($_GET['numero_int']) or isset($_GET['numero_ext'])))) {
        // On vérifie chacune des variables d'URL
        // Si elle existe, on la stocke dans une variable
        if (!empty($_GET['mail'])) {
            $getMail = strip_tags($_GET['mail']);
        }

        if (!empty($_GET['name'])) {
            $getName = strip_tags($_GET['name']);
        }

        if (!empty($_GET['pass'])) {
            $getPass = strip_tags($_GET['pass']);
        }

        if (!empty($_GET['priv'])) {
            $getPerm = strip_tags($_GET['priv']);
        }

        if (!empty($_GET['nom'])) {
            $getNom = strip_tags($_GET['nom']);
        }

        if (!empty($_GET['prenom'])) {
            $getPrenom = strip_tags($_GET['prenom']);
        }

        if (!empty($_GET['numero_int'])) {
            $getNumint = strip_tags($_GET['numero_int']);
        }

        if (!empty($_GET['numero_ext'])) {
            $getNumext = strip_tags($_GET['numero_ext']);
        }

        if (!empty($_GET['note'])) {
            $getNote = strip_tags($_GET['note']);
        }
    }
    else {
        header('Location: index.php?session'.$_SESSION['username'].'');
    }

    /* TODO : A utiliser pour les logs
     function time_to_readable($secs): string {
        $bit = array(
            ' an' => $secs / 31556926 % 12,
            ' semaine' => $secs / 604800 % 52,
            ' jour' => $secs / 86400 % 7,
            ' heure' => $secs / 3600 % 24,
            ' minute' => $secs / 60 % 60,
            ' seconde' => $secs % 60
        );

        foreach ($bit as $k => $v) {
            if ($v > 1) $ret[] = $v . $k . 's';
            if ($v == 1) $ret[] = $v . $k;
        }
        array_splice($ret, count($ret) - 1, 0, 'and');
        $ret[] = '';

        return join(' ', $ret);
    }
    $nowTime = time();
    $oldTime = 1653869267;
    $theTime = $nowTime-$oldTime;

    $humanReadable = time_to_readable($theTime);

    echo 'Voici un exemple : '.$humanReadable; */
?>

<!-- Bouton de retour au site -->
<div class="text-center">
    <a type="button" href="<?php if (isset($getMail)) { ?>users.php?session=<?php ?><?php } else { ?>contacts.php?session=<?php ?><?php }
    echo $_SESSION['username'] ?>" class="btn btn-primary mt-5">
        <- Retour au site
    </a>
</div>

<!-- Alignement de la colonne -->
<div class="row justify-content-center">
    <!-- On définit les dimensions -->
    <div class="col-xl-5 col-lg-2">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <?php
                            if (isset($getMail) or (isset($getName)) or (isset($getPass)) or (isset($getPerm))) {
                            ?>
                            <!-- Titre principal -->
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"><b>Modification : <u><?php echo $getName ?></u></b></h1>
                            </div>

                            <hr>

                            <!-- Formulaire de mise à jour des données d'un utilisateur -->
                            <form class="user" id="editForm" method="post" action="scripts/update_user.php">
                                <!-- Adresse mail input -->
                                <div class="form-group">
                                    <label for="formInputMail" class="form-label text-gray-900">Adresse mail</label>
                                    <input type="email" name="mailInput" class="form-control" id="formInputMail" aria-describedby="mailUpdate" value=<?php echo $getMail ?> onkeyup="refreshContent()">
                                </div>
                                <!-- Name input -->
                                <div class="form-group">
                                    <label for="formInputName" class="form-label text-gray-900">Nom d'utilisateur</label>
                                    <input type="text" name="nameInput" class="form-control" id="formInputName" aria-describedby="nameUpdate" value=<?php echo $getName ?> onkeyup="refreshContent()">
                                </div>
                                <!-- Password input -->
                                <div class="form-group">
                                    <label for="formInputPass" class="form-label text-gray-900">Mot de passe</label>
                                    <input type="password" name="passInput" class="form-control" id="formInputPass" placeholder="Password1234" value=<?php echo $getPass ?> onkeyup="refreshContent()">
                                </div>
                                <!-- Perm input -->
                                <div class="form-group">
                                    <label for="formInputPerm" class="form-label text-gray-900">Permissions</label>
                                    <input type="number" name="permInput" class="form-control" id="formInputPerm" aria-describedby="permUpdate" placeholder="1 = Utilisateur | 2 = Admin | 3 = Superadmin" value=<?php echo $getPerm ?> onkeyup="refreshContent()">
                                </div>
                                <input type="hidden" name="nameVerification" class="form-control" id="formInputHidden" aria-describedby="neededVar" value=<?php echo $getName ?>>
                            </form>

                            <hr>

                            <!-- Bouton de sauvegarde des données -->
                            <div class="text-center">
                                <button type="submit" form="editForm" class="btn btn-primary btn-user btn-block" id="updateSubmitBtn">
                                    Sauvegarder les modifications
                                </button>
                            </div>
                            <?php
                            }
                            ?>
                            <?php
                            if (isset($getNom) or (isset($getPrenom)) or (isset($getNumint)) or (isset($getNumext))) {
                                ?>
                                <!-- Titre principal -->
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4"><b>Modification : <u><?php echo "".$getPrenom." ".$getNom ?></u></b></h1>
                                </div>

                                <hr>

                                <!-- Formulaire de mise à jour des données d'un utilisateur -->
                                <form class="user" id="editForm" method="post" action="scripts/update_contact.php">
                                    <!-- Nom input -->
                                    <div class="form-group">
                                        <label for="formInputMail" class="form-label text-gray-900">Nom du contact</label>
                                        <input type="text" name="nomInput" class="form-control" id="formInputMail" aria-describedby="nomUpdate" value=<?php echo $getNom ?> onkeyup="refreshContent()">
                                    </div>
                                    <!-- Prénom input -->
                                    <div class="form-group">
                                        <label for="formInputMail" class="form-label text-gray-900">Prénom du contact</label>
                                        <input type="text" name="prenomInput" class="form-control" id="formInputMail" aria-describedby="prenomUpdate" value=<?php echo $getPrenom ?> onkeyup="refreshContent()">
                                    </div>
                                    <!-- Numéro interne input -->
                                    <div class="form-group">
                                        <label for="formInputName" class="form-label text-gray-900">Numéro interne</label>
                                        <input type="text" name="numIntInput" class="form-control" id="formInputName" placeholder="XXXX" aria-describedby="numintUpdate" value=<?php echo $getNumint ?> onkeyup="refreshContent()">
                                    </div>
                                    <!-- Numéro externe input -->
                                    <div class="form-group">
                                        <label for="formInputPass" class="form-label text-gray-900">Numéro externe</label>
                                        <input type="text" name="numExtInput" class="form-control" id="formInputPass" placeholder="06XXXXXXXX" aria-describedby="numextUpdate" value=<?php echo $getNumext ?> onkeyup="refreshContent()">
                                    </div>
                                    <!-- Note input -->
                                    <div class="form-group">
                                        <label for="formInputPerm" class="form-label text-gray-900">Commentaire</label>
                                        <input type="text" name="noteInput" class="form-control" id="formInputPerm" aria-describedby="noteUpdate" onkeyup="refreshContent()" value=<?php echo $getNote ?>>
                                    </div>
                                    <input type="hidden" name="nameVerification" class="form-control" id="formInputHidden" aria-describedby="neededVar" value=<?php echo $getNom ?>>

                                </form>

                                <hr>

                                <!-- Bouton de sauvegarde des données -->
                                <div class="text-center">
                                    <button type="submit" form="editForm" class="btn btn-primary btn-user btn-block" id="updateSubmitBtn">
                                        Sauvegarder les modifications
                                    </button>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>