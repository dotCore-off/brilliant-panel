<!-- Contenu de la page -->
<div class="container-fluid">
    <!-- En-tête de page -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <!-- Titre de la page -->
        <h1 class="h3 mb-0 text-gray-800 ml-auto mr-auto">Supervision - Paramètres</h1>
    </div>
</div>

<div class="row justify-content-center">
    <!-- On définit les dimensions -->
    <div class="col-xl-4 col-lg-3">
        <!-- Message du jour - Configuration -->
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <!-- Titre principal -->
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"><b>Configuration - Message du jour</b></h1>
                            </div>
                            <hr>
                            <?php
                            // On exécute une requête pour récupérer le contenu de la colonne motd_content
                            $motdQuery = $bdd->query('SELECT DISTINCT motd_content from acp_settings');

                            // On récupère le résultat de la requête ci-dessus
                            $results = $motdQuery->fetch(PDO::FETCH_ASSOC);

                            // On met fin à la requête pour libérer de la mémoire
                            $motdQuery->closeCursor();

                            $motdFinalContent = "";

                            // On boucle à travers les résultats pour éviter d'avoir une erreur Array => string conversion
                            foreach ($results as $message) {
                            // On stock dans la variable
                            $motdFinalContent = $message;
                            }
                            ?>
                            <!-- Formulaire pour le message du jour -->
                            <form class="motdcard mt-4" id="motdConfigForm" method="post" action="scripts/update_motd.php">
                                <div class="mb-3">
                                    <label for="motdArea" class="form-label text-gray-900">Contenu du message <sup>*</sup></label>
                                    <textarea class="form-control" id="motdArea" name="motdContentArea" rows="4" onkeyup="refreshContent()" placeholder="<?php echo $motdFinalContent ?>"></textarea>
                                </div>
                            </form>
                            <!-- Bouton de sauvegarde -->
                            <div class="text-center">
                                <button type="submit" form="motdConfigForm" class="btn btn-primary btn-user btn-block" id="motdSubmitBtn">
                                    Sauvegarder le MOTD
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <span class="text-gray-500 text-right">* Note : le bouton s'activera à partir de 20 caractères minimum.</span>
            </div>
        </div>
    </div>
    <!-- On définit les dimensions -->
    <div class="col-xl-4 col-lg-1">
        <!-- BDD - Configuration - TODO : Faire en sorte que ça fonctionne -->
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <!-- Titre principal -->
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"><b>Configuration - Base de données</b></h1>
                            </div>
                            <hr>
                            <!-- Formulaire pour les informations BDD -->
                            <form class="bddconnect" id="bddConfigForm" method="post" action="scripts/replace_config_db.php">
                                <!-- Host input -->
                                <div class="form-group input-group">
                                    <input type="text" name="hostBdd" class="form-control form-control-user" id="hostInput" placeholder="Host (ou localhost)" onkeyup="refreshContent()" value=<?php echo $BDD_HOST ?>>
                                </div>
                                <!-- User input -->
                                <div class="form-group input-group">
                                    <input type="text" name="userBdd" class="form-control form-control-user" id="userInput" placeholder="Nom d'utilisateur" onkeyup="refreshContent()" value=<?php echo $BDD_USERNAME ?>>
                                </div>
                                <!-- Mot de passe input -->
                                <div class="form-group">
                                    <input type="password" name="passBdd" class="form-control form-control-user" id="passInput" placeholder="Mot de passe" onkeyup="refreshContent()" value="*********">
                                </div>
                                <!-- BD input -->
                                <div class="form-group">
                                    <input type="text" name="dbBdd" class="form-control form-control-user" id="tableInput" placeholder="Nom de la base" onkeyup="refreshContent()" value=<?php echo $BDD_DB ?>>
                                </div>
                            </form>

                            <!-- Bouton de sauvegarde -->
                            <div class="text-center">
                                <button type="submit" form="bddConfigForm" class="btn btn-primary btn-user btn-block" id="bddSubmitBtn">
                                    Sauvegarder les modifications
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row justify-content-center">
    <?php
    if (!isset($_GET['code'])) {
    ?>
    <!-- Bouton de vérification de MàJ -->
    <div class="text-center">
        <a type="button" href="updater/fetch_version.php" class="btn btn-primary btn-user  btn-icon-split">
            <!-- Icône FA -->
            <span class="icon text-white-75">
                <i class="fas fa-sync"></i>
            </span>
            <!-- Texte -->
            <span class="text">Vérifier les mises à jour</span>
        </a>
    </div>
    <?php
    }
    if (isset($_GET['code']) and $_GET['code'] == "1") {
    ?>
    <!-- Bouton de téléchargement -->
    <div class="text-center">
        <button type="submit" form="bddConfigForm" class="btn btn-success btn-user  btn-icon-split">
            <!-- Icône FA -->
            <span class="icon text-white-75">
                <i class="fas fa-download"></i>
            </span>
            <!-- Texte -->
            <span class="text">Mettre à jour le panel</span>
        </button>
    </div>
    <?php
    }
    if (isset($_GET['code']) and $_GET['code'] == "0") {
    ?>
    <!-- Bouton désactivé-->
    <div class="text-center">
        <button type="submit" class="btn btn-danger btn-user  btn-icon-split disabled">
            <!-- Icône FA -->
            <span class="icon text-white-75">
                <i class="fas fa-times-circle"></i>
            </span>
            <!-- Texte -->
            <span class="text">Aucune mise à jour disponible</span>
        </button>
    </div>
    <?php
    }
    ?>
</div>