<!-- Contenu de la page -->
<div class="container-fluid">
<!-- En-tête de page -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <!-- Titre de la page -->
    <h1 class="h3 mb-0 text-gray-800 ml-auto mr-auto">Supervision - Base de données</h1>
</div>
<!-- Alignement de la colonne -->
<div class="row justify-content-center">
    <!-- On définit les dimensions -->
    <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                    <div class="col-lg-6">
                        <div class="p-5">
                            <!-- Titre principal -->
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"><b>Informations de connexion</b></h1>
                            </div>
                            <hr>
                            <!-- Formulaire pour les informations BDD -->
                            <form class="bddconnect" id="bddConfigForm" method="post" action="scripts/replace_config_db.php">
                                <!-- Host input -->
                                <div class="form-group input-group">
                                    <input type="text" name="hostBdd" class="form-control form-control-user" id="hostInput" placeholder="Host (ou localhost)" onkeyup="refreshContent()" value=<?php echo $BDD_HOST ?>>
                                </div>  
                                <!-- User input -->
                                <div class="form-group input-group">
                                    <input type="text" name="userBdd" class="form-control form-control-user" id="userInput" placeholder="Nom d'utilisateur" onkeyup="refreshContent()" value=<?php echo $BDD_USERNAME ?>>
                                </div>               
                                <!-- Mot de passe input -->
                                <div class="form-group">
                                    <input type="password" name="passBdd" class="form-control form-control-user" id="passInput" placeholder="Mot de passe" onkeyup="refreshContent()" value="*********">
                                </div>
                                <!-- BD input -->
                                <div class="form-group">
                                    <input type="text" name="dbBdd" class="form-control form-control-user" id="tableInput" placeholder="Nom de la base" onkeyup="refreshContent()" value=<?php echo $BDD_DB ?>>
                                </div>
                            </form>
                            
                            <!-- Bouton de sauvegarde -->
                            <div class="text-center">
                                <button type="submit" form="bddConfigForm" class="btn btn-primary btn-user btn-block" id="bddSubmitBtn">
                                    Sauvegarder les modifications
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>