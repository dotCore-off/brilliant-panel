<!-- Alignement de la colonne -->
<div class="row justify-content-center">
    <!-- On définit les dimensions -->
    <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                    <div class="col-lg-6">
                        <div class="p-5">
                            <!-- Titre principal -->
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"><b>Bienvenue sur le BP !</b></h1>
                            </div>
                            
                            <hr>

                            <!-- Formulaire de connexion principal - TODO : Ajout de labels -->
                            <form class="user" id="connectForm" method="post" action="scripts/login_system.php">
                                <!-- Adresse mail input -->
                                <div class="form-group">
                                    <input type="text" name="userInput" class="form-control form-control-user" id="inputLoginUsername" aria-describedby="nameHelp" placeholder="Votre nom d'utilisateur" onkeyup="refreshContent()">
                                </div>               
                                <!-- Mot de passe input -->
                                <div class="form-group">
                                    <input type="password" name="passInput" class="form-control form-control-user" id="inputLoginPassword" aria-describedby="passHelp" placeholder="Votre mot de passe" onkeyup="refreshContent()">
                                </div>
                            </form>
                            
                            <!-- Bouton de login -->
                            <div class="text-center">
                                <button type="submit" form="connectForm" class="btn btn-primary btn-user btn-block" id="loginSubmitBtn">
                                    Se connecter
                                </button>
                            </div>

                            <hr>
                            
                            <!-- Bouton pour le changement de mot de passe -->
                            <div class="text-center">
                                <button type="button" class="btn btn-danger btn-user btn-block" data-toggle="modal" data-target="#firstForm">
                                    Mot de passe oublié ?
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>