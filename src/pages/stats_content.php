<!-- Contenu de la page -->
<div class="container-fluid" id="backgroundBp">
    <!-- En-tête de page -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <!-- Titre de la page -->
        <h1 class="h3 mb-0 text-gray-800 ml-auto mr-auto">Supervision - Statistiques</h1>
    </div>

    <!-- Première ligne de contenu - TODO : Rendre les cartes cliquables -->
    <div class="row">
        <!-- Carte - Version -->
        <div class="col-xl-3 col-md-6 mb-4">
            <!-- Bordures -->
            <div class="card lift border-left-dark border-top-dark border-bottom-dark shadow h-100 py-2 card-cut3">
                <!-- Contenu de la carte -->
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <!-- Titre -->
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Version du panel</div>
                            <!-- Contenu principal -->
                            <div class="h5 mb-0 font-weight-bold text-gray-800">0.450</div>
                        </div>
                        <!-- Icône -->
                        <div class="col-auto">
                            <i class="fas fa-code-branch fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        // Requête de dénombrage
        $countQuery = "SELECT COUNT(username) FROM acp_users";

        // Préparation + Exécution
        $getUsers = $bdd->prepare($countQuery);
        $getUsers->execute();

        // Récupération du résultat + Conversion pour l'affichage
        $results = $getUsers->fetch(PDO::FETCH_NUM);
        $counts = $results[0];
        ?>

        <!-- Carte - Opérateurs -->
        <div class="col-xl-3 col-md-6 mb-4">
            <!-- Bordures -->
            <div class="card lift border-top-dark border-bottom-dark shadow h-100 py-2 card-cut1">
                <!-- Contenu de la carte -->
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <!-- Titre -->
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Nombre d'opérateur(s)</div>
                            <!-- Contenu principal -->
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $counts ?></div>
                        </div>
                        <!-- Icône -->
                        <div class="col-auto">
                            <i class="fas fa-users fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        // Requête de dénombrage
        $requestCount = "SELECT COUNT(email) FROM acp_requests";

        // Préparation + Exécution
        $getRequests = $bdd->prepare($requestCount);
        $getRequests->execute();

        // Récupération du résultat + Conversion pour l'affichage
        $results = $getRequests->fetch(PDO::FETCH_NUM);
        $counts = $results[0];
        ?>

        <!-- Carte - Requêtes en attente -->
        <div class="col-xl-3 col-md-6 mb-4">
            <!-- Bordures -->
            <div class="card lift border-top-dark border-bottom-dark shadow h-100 py-2 card-cut2">
                <!-- Contenu de la carte -->
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <!-- Titre -->
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Demande(s) en attente</div>
                            <!-- Contenu principal -->
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $counts ?></div>
                        </div>
                        <!-- Icône -->
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Carte - Stockage utilisé -->
        <div class="col-xl-3 col-md-6 mb-4">
            <!-- Bordures -->
            <div class="card lift border-top-dark border-bottom-dark border-right-dark shadow h-100 py-2 card-cut4">
                <!-- Contenu de la carte -->
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <!-- Titre -->
                            <div class="text-xs font-weight-bolder text-primary text-uppercase mb-1">Stockage libre</div>
                            <div class="row no-gutters align-items-center">
                                <?php
                                // Récupération de l'espace de disque libre
                                $getEmpty = disk_free_space("C:");

                                // Récupération de l'espace de disque total
                                $getTotal = disk_total_space("C:");

                                // Conversion en pourcentage
                                $longVar = ($getEmpty * 100) / $getTotal;

                                // Suppression des nombres derrière la virgule
                                $equivalent = $longVar;
                                $humanReadable = explode(".",$equivalent);
                                unset($humanReadable[1]);
                                $humanReadable = implode(".",$humanReadable);

                                ?>
                                <!-- Contenu principal -->
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo $humanReadable ?>%</div>
                                </div>
                                <!-- Barre de progression -->
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <!-- Avancement + Style -->
                                        <div class="progress-bar-animated progress-bar-striped bg-primary" role="progressbar" style="width: <?php echo $humanReadable ?>%" aria-valuenow="<?php echo $humanReadable ?>" aria-valuemin="0" aria-valuemax="100" aria-label="Espace de stockage libre"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Icône -->
                        <div class="col-auto">
                            <i class="fas fa-database fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de la première ligne de contenu -->

        <!-- Deuxième ligne de contenu - TODO : Prevent first card width reduction + Change content -->
        <div class="row">
            <!-- Carte de présentation -->
            <div class="col-xl-5 col-md-6 mb-4 ml-lg-auto mr-lg-auto">
                <!-- Description -->
                <div class="card border-bottom-primary shadow mb-4 position-sticky">
                    <a href="#collapseDesc" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseDesc">
                        <h6 class="m-0 font-weight-bold text-primary">Bienvenue sur le Brilliant Panel !</h6>
                    </a>
                    <div class="collapse show" id="collapseDesc">
                        <div class="card-body">
                            <p class="mt-0 mb-2 text-gray-800"><strong>Brilliant<sup>Panel</sup></strong> est une interface web <b>flexible et intelligente</b>
                                permettant de gérer efficacement l'ensemble de votre parc téléphonique,
                                tout en implémentant de nombreuses fonctionnalités externes.
                            </p>
                            <p class="mb-2 text-gray-800"><b>Légère, claire et modulable</b>, Brilliant<sup>P</sup> permet entre autre
                                l'intégration d'autres éléments grâce à son <b>code simple et expliqué</b>
                                ainsi que ses documentations techniques <b>compréhensibles et pensées pour tous</b>.<br>
                            </p>
                            <p class="mb-1 text-gray-800">
                                <strong>En vous souhaitant une agréable navigation !</strong>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Système de gestion de statistiques - TODO : Rendre les valeurs dynamiques et les déplacer sur une autre page -->
            <div class="col-xl-5 col-md-6 mb-4 ml-lg-auto mr-lg-auto">
                <!-- Système de gestion de statistiques -->
                <div class="card border-bottom-primary shadow mb-4">
                    <!-- Statistiques - Header -->
                    <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
                        <h6 class="m-0 font-weight-bold text-primary">Statistiques du serveur</h6>
                    </a>
                    <!-- Statistiques - Contenu -->
                    <div class="collapse show" id="collapseCardExample">
                        <div class="card-body">
                            <!-- Texte -->
                            <?php
                            // Ping des serveurs DNS de Google
                            // $ip_address = '8.8.8.8';
                            // $result = exec('ping -n 2 -w 2 ' .$ip_address. '');
                            ?>
                            <h4 class="small font-weight-bold text-gray-800">Temps de réponse<span class="float-right">135 ms</span></h4>
                            <!-- Barre de progression -->
                            <div class="progress mb-4">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 13%"
                                     aria-valuenow="134" aria-valuemin="0" aria-valuemax="999" aria-label="Temps de réponse du serveur">
                                </div>
                            </div>
                            <h4 class="small font-weight-bold text-gray-800">Utilisation du CPU <span class="float-right">3%</span></h4>
                            <div class="progress mb-4">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 3%"
                                     aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" aria-label="Utilisation du processeur">
                                </div>
                            </div>
                            <h4 class="small font-weight-bold text-gray-800">Utilisation de la RAM <span class="float-right">1.1 GiB</span></h4>
                            <div class="progress mb-4">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 8%"
                                     aria-valuenow="8" aria-valuemin="0" aria-valuemax="100" aria-label="Utilisation de la RAM">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin du contenu principal -->
</div>