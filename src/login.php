<?php
    // Intégration de la config
    include_once('configuration/db.php');

    // Session toujours valide > Retour à l'index
    if (isset($_SESSION['username'])) {
        header('Location: index.php?session='.$_SESSION['username'].'');
    }
?>

<!DOCTYPE html>
<html lang="fr">
<!-- 
    Système créé par Alexis 'dotCore' Badel pour Habitat Métropole
            Reproduction complète ou partielle interdite
                    Tous droits réservés 2021
-->
<head>
    <!-- Elements de base -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Page de connexion au panel d'administration">
    <meta name="author" content="Alexis 'dotCore' Badel">
    <title>BP - Login</title>

    <!-- Importation des polices d'écriture -->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Importation des fichiers de style -->
    <link href="assets/css/style.min.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/avatar_user.png">

</head>

<!-- Background principal -->
<body class="bg-gradient-dark">
    <!-- Conteneur principal -->
    <div class="container">

        <!-- Intégration - Contenu -->
        <?php include("pages/login_content.php") ?>

    </div>

    <!-- Intégration - Formulaire de demande de reset par mail -->
    <?php include("forms/form_passresetone.php") ?>
    <?php include("forms/form_passresettwo.php") ?>

    <!-- Intégration - Script de vérification des formulaires -->
    <?php include("scripts/verify.php") ?>

    <!-- Intégration - Script de vérification d'environnement -->
    <?php include("scripts/check_env.php") ?>
    <?php include("scripts/notifications.php") ?>

    <!-- Scripts importants -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="assets/js/panel.min.js"></script>
</body>

</html>