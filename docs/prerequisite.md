# Guide d'installation - Pré-requis
**Bienvenue sur Brilliant Panel et merci beaucoup de vouloir l'utiliser.** \
Lors de son développement, ce dernier a été pensé pour être **installable sur un maximum d'environnements** et ce, peu importe la configuration.

Malgré cela, il y a **quelques petits pré-requis** à compléter en amont de l'installation.

## 🖥 Pré-requis du serveur hôte
**Il n'y a pas réellement de pré-requis** concernant le serveur qui hébergera le site. \
\
En revanche, nous vous conseillons de choisir tout en vous assurant **d'être en mesure de répondre aux pré-requis et diverses dépendances.** \
Les environnements sous **[UNIX](https://fr.wikipedia.org/wiki/Unix#Principales_familles_de_syst%C3%A8mes_UNIX) sont à privilégier** pour la facilité et la disponibilité de **certains paquets** _(OpenLDAP, etc..)_. \
Vous pouvez par exemple passer par des distributions [Linux](https://fr.wikipedia.org/wiki/Linux) comme [Debian](https://www.debian.org/distrib/) ou encore [Ubuntu](https://www.ubuntu-fr.org/download/).

## 🌐 Pré-requis de l'environnement web
**Voici les pré-requis nécessaires à l'installation du Brilliant Panel sur un environnement de production :** 
- **PHP**
> Version minimum : **7.3 ou ultérieure** \
> Note : important à respecter pour des raisons de **sécurité et de compatibilité**

Téléchargement de PHP disponible sur [cette page](https://www.php.net/downloads).

- **Base de données SQL**
> Possibilités : **MariaDB** / **MySQL** / **PostgreSQL** / **Oracle** _(ou autre driver propriétaire)_ \
> Note : **PDO** est utilisé pour l'ensemble des requêtes et est **compatible avec la plupart des drivers SQL**

Documentations de [MariaDB](https://mariadb.com/kb/en/documentation/), [MySQL](https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/), [PostgreSQL](https://www.postgresql.org/docs/) & [Oracle](https://docs.oracle.com/en/database/).

- **Serveur web**
> Possibilités : **Apache2** / **NGINX** \
> Utilisez selon vos préférences et vos disponibilités, je recommande personnellement **l'utilisation d'Apache2** car NGINX requiert une **configuration supplémentaire**

Documentations de [Apache2](https://httpd.apache.org/docs/2.4/fr/) & [NGINX](https://nginx.org/en/docs/). \
Pour plus d'informations concernant la **configuration du serveur web**, rendez vous sur [cette page](web.md).


## 🛠 Les environnements pré-fabriqués
Si besoin, vous pouvez directement utiliser des environnements permettant la **création et l'installation de sites web**. \
Ces derniers complètent la **plupart des pré-requis** listés ci-dessus et comprennent un **serveur web**, une **base de données** et **PHP**. \
\
**Voici ceux que je vous recommande ainsi que la configuration pour MAMP :**
- **MAMP** _(Windows / Mac)_ => [Lien de téléchargement](https://www.mamp.info/en/downloads/)
> Configuration disponible dans **MAMP > Preferences** \
> **Start / Stop** > ✔️ Start Servers when starting MAMP | ✔️ At startup open _(assure la disponibilité après un reboot / shutdown)_ \
> **Ports** > Changez selon les **disponibilités des ports** \
> **PHP** > Cache = **off** _(pour éviter tout problème avec les sessions)_ \
> **Web Server** > WebServer = **Apache** _(NGINX peut mal tourner sous MAMP)_ | Document root = ``...\brilliant-panel\src``

Pour vous connecter au panel, allez sur [localhost](http://localhost:80) ou sur ``http://ip-de-la-machine-hôte/``.

- **WampServer** _(Windows)_ => [Lien de téléchargement](https://www.wampserver.com/)
- **LAMP** _(Linux)_ => [Documentation pour Debian](https://wiki.debian.org/fr/Lamp) / [Documentation pour Ubuntu](https://doc.ubuntu-fr.org/lamp)


## ⛓ Les dépendances
Le projet possède **quelques dépendances** utilisées pour certains des éléments qui le composent. \
**Les dépendances principales sont :**
- [TataJS](https://www.cssscript.com/toast-prompt-tata/)
- [Font Awesome](https://fontawesome.com/)
- [jQuery](https://jquery.com/)
- [PHP LDAP]()

La **liste complète des dépendances** est trouvable dans le fichier ``brilliant-panel/src/package.json``.\
\
**Voici le contenu de ce fichier :**
```json
{
    "title": "SB Admin 2",
    "name": "startbootstrap-sb-admin-2",
    "version": "4.1.3",
    "scripts": {
        "start": "node_modules/.bin/gulp watch"
    },
    "description": "An open source Bootstrap 4 admin theme.",
    "keywords": [
        "css",
        "sass",
        "html",
        "responsive",
        "theme",
        "template",
        "admin",
        "app"
    ],
    "homepage": "https://startbootstrap.com/theme/sb-admin-2",
    "bugs": {
        "url": "https://github.com/StartBootstrap/startbootstrap-sb-admin-2/issues",
        "email": "feedback@startbootstrap.com"
    },
    "license": "MIT",
    "author": "Start Bootstrap",
    "contributors": [
        "David Miller (https://davidmiller.io/)"
    ],
    "repository": {
        "type": "git",
        "url": "https://github.com/StartBootstrap/startbootstrap-sb-admin-2.git"
    },
    "dependencies": {
        "@fortawesome/fontawesome-free": "5.15.1",
        "bootstrap": "4.5.3",
        "chart.js": "2.9.4",
        "datatables.net-bs4": "1.10.22",
        "jquery": "3.5.1",
        "jquery.easing": "^1.4.1"
    },
    "devDependencies": {
        "browser-sync": "2.26.13",
        "del": "6.0.0",
        "gulp": "4.0.2",
        "gulp-autoprefixer": "7.0.1",
        "gulp-clean-css": "4.3.0",
        "gulp-header": "2.0.9",
        "gulp-plumber": "^1.2.1",
        "gulp-rename": "2.0.0",
        "gulp-sass": "4.1.0",
        "gulp-uglify": "3.0.2",
        "merge-stream": "2.0.0"
    }
}
```

**Vous souhaitez pouvoir gérer une base de contacts téléphoniques via un annuaire LDAP ?** \
Brilliant Panel inclut par défaut des scripts permettant la **connexion et la synchronisation** entre une base de données
 **MySQL** et un **annuaire LDAP**.
\
Rendez vous dans la [section suivante](prerequisite?id=📠-la-librairie-php-ldap) pour voir comment installer correctement la **librairie PHP LDAP**.

Autrement, vous pouvez déjà [installer le panel](installation.md) !

## 📠 La librairie php-ldap
La librairie php-ldap vous permettra de gérer un annuaire / une base de contacts téléphonique depuis votre interface web.

**Installation sous Windows :**
> - s'assurer d'avoir un [environnement web complet](prerequisite?id=🌐-pré-requis-de-l39environnement-web)
> - **installer** [OpenLDAP](https://www.openldap.org/software/download/) sur votre serveur
> - **obtenir la DLL** ``libsasl.dll`` et faire en sorte qu'elle soit accessible via une **PATH**
> > Si besoin, se référer à [cette documentation](https://www.php.net/manual/fr/faq.installation.php#faq.installation.addtopath) pour plus d'informations
> - **ajouter l'option de configuration** ``--with-ldap[=DIR]`` à la compilation de votre PHP
> > Si besoin, se référer à [cette documentation](https://www.php.net/manual/fr/ldap.requirements.php) pour plus d'informations

**Installation sous Linux :**
> - s'assurer d'avoir un [environnement web complet](prerequisite?id=🌐-pré-requis-de-l39environnement-web)
> - **mettre à jour** le système via ``apt update / apt upgrade``
> - **installer le paquet LDAP PHP** via la commande ``apt-get install phpX-ldap``
> > **Important** : remplacer le **X** par la **version de PHP**, par exemple ``apt-get install php7.4-ldap``
> - **ajouter l'option de configuration** ``--with-ldap[=DIR]`` à la compilation de votre PHP
> > Si besoin, se référer à [cette documentation](https://www.php.net/manual/fr/ldap.requirements.php) pour plus d'informations
> - **Rédémarrer le serveur web**, LDAP PHP devrait être installé

**Tout est bon ?** \
Alors il est l'heure [d'installer le panel](installation.md) !