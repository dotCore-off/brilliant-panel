# Guide d'installation - Configuration de la base de données
Une fois que votre panel est **correctement installé**, il faudra le **configurer pour pouvoir le rendre totalement fonctionnel**. \
La configuration est relativement **aiguillée** et ne dure en général que **quelques minutes**. \
\
**Suivez tout de même les instructions ci-dessous pour ne pas faire d'erreur.**

## 📊 Pourquoi et comment la configurer ?
Dans un premier temps, il convient de **configurer correctement les informations de connexion à la BDD**. \
En effet, Brilliant Panel en a besoin pour **stocker et récupérer tout un tas de données**, comme les utilisateurs ou encore les contacts. \
Il est donc **crucial** d'avoir une [base de données fonctionnelle](prerequisite.md) avant de tenter une quelconque configuration.

### ➡️ Changer les informations de connexion
Tout ce que vous avez à faire, c'est modifier les identifiants de connexion pour qu'elles correspondent à votre base de données. \
Pour se faire, rendez vous dans le fichier ``configuration/db.php``, à la ``ligne 11`` :
```php
📄 Fichier: configuration/db.php | Ligne 11

// Valeurs par défaut
$BDD_HOST = 'localhost';
$BDD_USERNAME = 'root';
$BDD_PASS = 'root';
$BDD_DB = 'bdd';
```
**Voici une petite explication des variables :**
- ``$BDD_HOST`` correspond au **serveur sur lequel est hébergée la base de données**
> **Valeur attendue : adresse IP ou nom d'hôte** \
> **Note :** si le serveur web **est localisé sur le même serveur** que le serveur SQL, laissez **localhost ou 127.0.0.1**
- ``$BDD_USERNAME`` correspond au nom de l'utilisateur qui aura l'accès à la base de données
> **Valeur attendue : nom d'utilisateur** \
> **Note :** assurez vous que cet utilisateur **possède les privilèges suffisants** sur cette base de données
- ``$BDD_PASS`` correspond au mot de passe de l'utilisateur inscrit dans ``$BDD_USERNAME``
> **Valeur attendue : mot de passe**
- ``$BDD_DB`` correspond à la base de données qui accueillera les données et requêtes du panel
> **Valeur attendue : base de données**

**Une fois la configuration terminée, sauvegardez les modifications effectuées** sans rien toucher d'autres. \
Dirigez-vous à [la section suivante](configuration?id=📝-installation-de-la-base-de-données) pour **terminer l'installation du panel**.


## 📝 Installation de la base de données
La connexion à la base de données a été **correctement effectuée**, mais celle-ci est pour le moment **vide** _(si vous utilisez une base de données déjà remplie, passez ici)_. \

Il va falloir **installer l'ensemble des tables et colonnes** afin d'assurer le bon fonctionnement du panel, et ça tombe bien puisqu'il y a un script dédié à cela. \
En effet, **rendez-vous à l'adresse** ``https://ip-ou-nom-de-domaine/install.php``. \
Cela aura pour effet **d'activer le script d'initialisation de la DB** qui devrait être très rapide et vous devriez être rediriger sur la page de connexion. \
Pensez à vérifier, via [phpMyAdmin](https://www.phpmyadmin.net/) par exemple, si la base de données s'est **correctement importée**. \
**Vous devriez voir ceci :** \
![img.png](img/img.png)
\
Ce n'est pas le cas ? \
**Importez directement [ce fichier](https://getbp.dotcore-lab.net/sql_import_bp.zip)** dans votre base de données ! \
> Le login est le même que pour les fichiers du panel, trouvable [ici](installation?id=📥-téléchargement-des-fichiers)

Pour vérifier la bonne installation du panel et de sa base de données, **rendez-vous à l'adresse** ``https://ip-ou-nom-de-domaine/login.php`` et **connectez vous** avec le compte suivant :
> Identifiant : Administrateur | Mot de passe : Admin12345 _(c'est le compte créé par défaut)_

Si vous êtes redirigé sur le Dashboard _(index.php)_, **c'est que tout fonctionne correctement**, vous pouvez donc passer à [la suite et fin de la configuration](extraconfig.md). \
Autrement, vous trouverez dans la [section suivante](configuration?id=❌-origines-possibles-d39une-erreur-sql) les **différentes raisons qui peuvent conduire à un problème avec SQL.**

## ❌ Origines possibles d'une erreur SQL
Ci-dessous, vous trouverez la **liste des raisons possibles d'une erreur avec SQL**. \
Si aucune des pistes de résolution ne fonctionne, [contactez le support](mailto:private@dotcore-lab.net). \
\
**Liste des pistes de résolution :**
- vérifiez que vous avez entré les **bonnes informations** dans la ``configuration/db.php``
- **vérifiez l'intégrité** du fichier ``configuration/db.php`` _([la version par défaut est disponible ici](configuration?id=✔%ef%b8%8f-configuration-par-défaut-sql))_
- vérifiez que votre base de données **accepte les connexions à distance** _([documentation pour MySQL](https://phoenixnap.com/kb/mysql-remote-connection))_
- vérifiez que l'utilisateur sélectionné **possède bien les bons privilèges** sur la base de données
- vérifiez dans la **configuration du pare-feu** que le port de connexion à la base de données **autorise le traffic entrant / sortant** _([port par défaut de MySQL](https://kinsta.com/fr/base-de-connaissances/port-mysql/#:~:text=Le%20port%203306%20est%20le,utilis%C3%A9%20pour%20le%20protocole%20MySQL.))_
> Note : il est préférable de passer par d'autres méthodes telle que SSH, qui sont souvent plus sécurisées
- **mettez à jour les drivers** de votre base de données


## ✔️ Configuration par défaut (db.php)
**Voici la configuration par défaut du fichier** ``configuration/db.php`` :
```php
📄 Fichier: configuration/db.php

<?php
// Démarrage du système de session
session_start();

/*
        Variables de connexion à la base de données
    A modifier avec les informations de votre environnement
        Remplissez soigneusement les données ci-dessous
*/
$BDD_HOST = 'localhost';
$BDD_USERNAME = 'root';
$BDD_PASS = 'root';
$BDD_DB = 'backup_test';

// Connexion à la base de données
try {
    $bdd = new PDO('mysql:host='.$BDD_HOST.';dbname='.$BDD_DB.';charset=utf8', ''.$BDD_USERNAME.'', ''.$BDD_PASS.'');
}

// En cas d'erreur, on l'affiche
catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}
```