- Brilliant Panel

    - [Introduction](README.md)

- Guide d'installation
  
    - [Pré-requis](prerequisite?id=guide-d39installation-pré-requis)
    - [Installation du panel](installation?id=guide-d39installation-web-panel)
    - [Configuration de la BDD](configuration?id=guide-d39installation-configuration-de-la-base-de-données)
    - [Configurations supplémentaires](extraconfig?id=guide-d39installation-configurations-supplémentaires)

- Guide d'utilisation
    - [Première approche](connexion.md)