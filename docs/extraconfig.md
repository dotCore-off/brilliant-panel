# Guide d'installation - Configurations supplémentaires
Ca y est, le panel est **entièrement fonctionnel** si vous avez **correctement suivi** [la configuration de la BDD](configuration.md). \
**Totalement fonctionnel ? Et bien non, pas encore.** \
Il reste **un fichier de configuration** dont nous n'avons pas encore parlé, **et il est l'heure de se pencher dessus.**

## 🔨 Le fichier config.php
Le fichier ``config.php`` est le deuxième et dernier fichier de configuration du Brilliant Panel. \
Il comporte quelques variables plutôt utiles que nous utilisons au sein du panel, mais aussi d'autres plus mystérieuses. \
Cela ne signifie pas pour autant qu'il faut le négliger tant son utilisé deviendra de plus en plus importe au fil du temps. \
\
Ce fichier se situe donc dans ``configuration/config.php``, et ne comporte à ce jour que trois variables que nous détaillons [dans la section suivante]().

## 💾 Ses variables, explications et utilités
Tout comme l'ensemble des fichiers du projet, et plus particulièrement le fichier ``configuration/db.php``, le fichier ``config.php`` est relativement bien documenté. \
Il peut être assez simple de comprendre ce que fait chacune de ses variables, mais une explication peut toujours servir, surtout pour certaines.\

A ce jour _(04/06)_, en [version de build 0.4](https://gitlab.com/dotCore-off/brilliant-panel), le fichier comporte **trois variables** :
- **la variable** ``$ENV_TYPE``
> Elle est de type **string** _(chaîne de caractères)_ et ne peut contenir que **deux valeurs** : ``production`` ou ``dev`` \
> Elle permet de **définir le type d'environnement** sur lequel est installé le panel, lui permettant **d'adopter certains comportements particuliers** \
> Note : la valeur ``dev`` **n'est pas supportée** à l'heure actuelle et pourra causer des **soucis de sécurité**, laissez donc ``production`` dans tous les cas
- **la variable** ``$DISK_PATH``
> Elle est également de type **string** et peut comporter **n'importe quelle valeur** tant que cette dernière est **valide** \
> Elle correspond au **répertoire maître du serveur**, et permet donc de connaître le **stockage total** de ce dernier, ainsi qu'en déduire le **stockage libre** \
> Note : des variables telles que ``C:``  _(Windows)_ ou ``/`` _(Linux)_ sont **valides** par exemple! \
> Note 2 : si la valeur est **invalide**, vous devriez voir [ce genre d'erreurs](http://localhost:3000/img/img_2.png)
- **la variable** ``$LICENSE_KEY``
> Sans doute **LA** variable la plus mystérieuse, elle est de type **string** et correspond au **résultat de multiples encryptions** dont la dernière est effectuée en **base64** \
> Comme indiqué dans les commentaires du fichier, **ne touchez jamais à cette variable**, cela risque de **casser totalement le panel** \
> Cette variable permet de **vérifier l'authenticité de l'installation en local auprès de mon serveur de déploiement / build distant** \
> Elle **n'est pas encore utile à ce jour**, mais elle permettra notamment d'ajouter un module pour **mettre à jour le panel directement depuis ce dernier** sans avoir à re-télécharger les fichiers \
> Note : la clé est générée **automatiquement** au moment du build selon les valeurs que je lui donne et est **unique**

Et voilà, vous savez désormais tout sur la configuration du panel ! \
En cas de besoin, vous pouvez également lire [le guide d'utilisation]() pour en savoir plus sur le panel.