# Brilliant Panel - Un panel de gestion moderne et ergonomique

## 📑 Description
Cette interface web, qui est **en cours de création**, est réalisée dans le cadre de mon projet de stage au sein de [Habitat-Métropole](https://www.habitat-metropole.fr/).
Elle a été entièrement pensé pour répondre à une problématique actuelle : **la gestion**, tout en incorporant des éléments spécifiques à mon objectif de stage.

## ⚙️Caractéristiques
**BrillantCP** comprend actuellement :
- un système de **connexion complet et sécurisé**
- un **dashboard** contenant diverses informations utiles et dynamiques
- un système de **gestion des utilisateurs** *(création, gestion)*
- un système de **"mot de passe oublié"** *(gestion des requêtes, mail => WIP)*
- un système de **configuration des informations de la base de données**
- un système de **paramètrage du panel** *(MOTD, alerte, etc..)*
- un système de **changement de thème dynamique** *(clair/sombre => WIP)*
- un système de **permissions utilisateur simple et sécurisé**
- des **éléments ré-utilisables et dynamiques** *(topbar, navbar)*
- une **dizaine de scripts PHP optimisés rendant le tout fonctionnel**

## 📆 Planning
Ce panel est en constante évolution, et voici ce qui est prévu :
- une page permettant la **gestion des contacts via LDAP**
- un **système de logs avancé**

## 🌐 Liens utiles
Divers liens qui peuvent vous être utile :
- [Mon site](https://dotcore-lab.net/)
- [Roadmap du projet](https://trello.com/b/z9DrupFq/projet-de-stage-habmet)
- [Démo du panel](https://dev.dotcore-lab.net/)
> ID : Github   |   Password : Password1234

## ⭐ Crédits
Voici les différents outils nécessaires à l'existence du panel :
- [DataTables](https://datatables.net/manual/) pour **l'affichage des tables de données**
- [Bootstrap](https://getbootstrap.com/) en tant que **framework principal**
- [Start Bootstrap !](https://startbootstrap.com/) pour **le design** de base
- [TataJS](https://www.cssscript.com/toast-prompt-tata/) pour **les notifications intéractives**
- [Font Awesome](https://fontawesome.com/) pour **les icônes** sur le site
- [jQuery](https://jquery.com/) pour la plupart des **scripts et requêtes**
