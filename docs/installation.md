# Guide d'installation - Web Panel
Une fois que les pré-requis du projet ont été **vérifiés et complétés**, vous êtes en mesure **d'installer le Brilliant Panel**. \
Rassurez-vous, c'est plutôt **simple** si vous suivez **correctement** les instructions suivantes.

## 📥 Téléchargement des fichiers
Dans un premier temps, il faudra que vous **téléchargiez les fichiers du projet**. \
Il y a **différents moyens** à votre disposition dont je détaille le fonctionnement ci-dessous :
- **télécharger via le site dédié**
> - **Rendez vous** à [cette adresse](http://getbp.dotcore-lab.net/) et **connectez vous**
> - **Informations de connexion** => **ID** : AdminHMOPH | **Password** : @dmin2021OPH
> - Le site devrait vous **rediriger automatiquement** et le fichier **devrait être télécharger**
> - **Si ce n'est pas le cas, rendez-vous** à [cette adresse](http://getbp.dotcore-lab.net/brilliant_panel_d417.rar)

- **télécharger via** [Gitlab](https://gitlab.com/)
> - **Rendez vous** sur [Gitlab](https://gitlab.com/users/sign_in) et **connectez vous**
> - **Rendez vous** sur la [page du repository](https://gitlab.com/dotCore-off/brilliant-panel)
> - **Cliquez** sur [le bouton de téléchargement et choisissez le format souhaité](https://i.imgur.com/w69vg2M.png)
> - Les fichiers **seront enregistrés** sous le nom de ``brilliant-panel-main.extension``

## 📤 Extraction et installation
**Une fois le téléchargement effectué**, vous devriez avoir une **archive compressée** _(au format RAR, ZIP ou encore TAR)_. \
**Voici l'arborescence des fichiers :**
```md
📂 brilliant-panel
|_📄 README.md
|_📂 .git (contient les fichiers relatifs au versioning)
   |_ ... (utilité très limitée)
|_📂 .idea (contient les fichiers relatifs au développement / à l'IDE)
   |_ ... (inutile sauf en cas de développement sous PHPStorm)
|_📂 docs (contient les fichiers de la documentation)
   |_ ... (on détaillera son contenu plus tard)
|_📂 src (contient les fichiers du panel web)
   |_📂 assets (contient tous les fichiers ressources, dossier le plus lourd)
   |_📂 configuration (contient les fichiers de configuration)
   |_📂 elements (contient les divers éléments réutilisés plusieurs fois dans le panel)
   |_📂 forms (contient l'ensemble des formulaires)
   |_📂 pages (contient le contenu de chaque page)
   |_📂 scripts (contient l'ensemble des scripts en PHP / JS)
   |_ ... (index.php, edit.php, package.json, etc..)
```

**Pour installer le panel, voici les étapes à suivre :**
1. **Extraire le dossier** ``brilliant-panel/src`` de l'archive
2. **Déplacer l'ensemble du contenu du dossier** ``src`` à la racine de votre serveur web / à l'emplacement souhaité

**Vous devriez donc avoir ceci :** \
![img_1.png](img/img_1.png)

**Et voilà, c'est terminé !** \
Le Brilliant Panel est désormais [prêt à être configurer](configuration.md).